#!/usr/bin/env bash
set -euo pipefail

compton &

while true; do
    feh --randomize --bg-scale /home/julien/.local/wallpapers/penguins/*
    sleep 60
done &

xrandr --output DP-3 --primary  --mode 1920x1080 --pos 1920x0 --rotate normal --output DP-4 --mode 1920x1080 --pos 0x0 --rotate normal
