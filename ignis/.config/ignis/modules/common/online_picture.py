import requests
from ignis.widgets import Widget
from gi.repository import GdkPixbuf


def dowload_image(url: str) -> GdkPixbuf.Pixbuf:
    response = requests.get(url)
    loader = GdkPixbuf.PixbufLoader.new_with_type("jpeg")
    loader.write(response.content)
    loader.close()
    return loader.get_pixbuf()


class OnlinePicture(Widget.Picture):
    def __init__(self, url: str | None) -> None:
        super().__init__(image="", width=64, height=64)
        if url:
            pixbuf = dowload_image(url)
            self.set_property("image", pixbuf)

