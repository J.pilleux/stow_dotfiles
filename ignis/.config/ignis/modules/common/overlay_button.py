from ignis.app import IgnisApp
from ignis.widgets import Widget


app = IgnisApp.get_default()


class OverlayButton(Widget.Button):
    def __init__(self, namespace: str, extra_css: str = "") -> None:
        super().__init__(
            vexpand=True,
            hexpand=True,
            can_focus=False,
            css_classes=["unset", extra_css],
            on_click=lambda _: app.close_window(namespace),
        )
