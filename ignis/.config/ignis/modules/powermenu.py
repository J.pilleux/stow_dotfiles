from typing import Callable
from ignis.widgets import Widget
from ignis.app import IgnisApp
from ignis.utils import Utils
from modules.common.overlay_button import OverlayButton


app = IgnisApp.get_default()


class PowermenuButton(Widget.Box):
    def __init__(self, icon_name: str, label: str, on_click: Callable) -> None:
        super().__init__(
            child=[
                Widget.Button(
                    child=Widget.Icon(image=icon_name, pixel_size=36),
                    on_click=on_click,
                    css_classes=["powermenu-button", "unset"],
                ),
                Widget.Label(label=label, css_classes=["powermenu-button-label"]),
            ],
            vertical=True,
            css_classes=["powermenu-button-box"],
        )


def powermenu() -> None:
    shutdown_btn = PowermenuButton("system-shutdown", "Shutdown", _poweroff)
    reboot_btn = PowermenuButton("system-reboot-symbolic", "Reboot", _reboot)
    log_out_btn = PowermenuButton("system-log-out-symbolic", "Log out", _log_out)
    lock_screen = PowermenuButton("system-lock-screen-symbolic", "Lock", _lock)

    box1 = Widget.Box(child=[shutdown_btn, reboot_btn])
    box2 = Widget.Box(child=[log_out_btn, lock_screen])

    main_box = Widget.Box(
        vertical=True,
        valign="center",
        halign="center",
        css_classes=["powermenu-box"],
        child=[box1, box2],
    )

    overlay_button = OverlayButton("ignis_POWERMENU", "blurred_overlay")

    overlay = Widget.Overlay(child=overlay_button, overlays=[main_box])

    Widget.Window(
        popup=True,
        namespace="ignis_POWERMENU",
        kb_mode="on_demand",
        visible=False,
        anchor=["left", "right", "top", "bottom"],
        exclusivity="ignore",
        css_classes=["powermenu-window"],
        child=overlay,
    )


def _poweroff(*args) -> None:
    Utils.exec_sh_async("poweroff")


def _reboot(*args) -> None:
    Utils.exec_sh_async("reboot")


def _lock(*args) -> None:
    app.close_window("ignis_POWERMENU")
    Utils.exec_sh_async("systemctl suspend && hyprlock")


def _log_out(*args) -> None:
    Utils.exec_sh_async("hyprctl dispatch exit 0")
