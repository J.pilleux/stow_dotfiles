import dbus
from typing import Callable
from ignis.utils import Utils
from ignis.widgets import Widget
from modules.common.online_picture import OnlinePicture, dowload_image
from modules.common.overlay_button import OverlayButton


class MediaButton(Widget.Box):
    def __init__(self, icon_name: str, on_click: Callable) -> None:
        super().__init__(
            child=[
                Widget.Button(
                    child=Widget.Icon(image=icon_name, pixel_size=16),
                    css_classes=["media-button", "unset"],
                    on_click=on_click,
                ),
            ],
            vertical=True,
            css_classes=["media-button-box"],
        )


class SongData(Widget.Box):
    def __init__(self, title: str, artist: str, album: str) -> None:
        self._title = Widget.Label(label=title, css_classes=["song-title"])
        self._artist = Widget.Label(label=artist, css_classes=["song-artist"])
        self._album = Widget.Label(label=album, css_classes=["song-album"])
        super().__init__(child=[self._title, self._artist, self._album], vertical=True, halign="center", valign="center")

    def set_title(self, title: str) -> None:
        self._title.set_property("label", title)

    def set_artist(self, artist: str) -> None:
        self._artist.set_property("label", artist)

    def set_album(self, album: str) -> None:
        self._album.set_property("label", album)


class SongBox(Widget.Box):
    def __init__(self) -> None:
        self._song_data = SongData("Song title", "Artist", "Album")
        self._picture = OnlinePicture(None)
        super().__init__(child=[self._picture, self._song_data], vertical=False, spacing=52)

    def set_title(self, title: str) -> None:
        self._song_data.set_title(title)

    def set_artist(self, artist: str) -> None:
        self._song_data.set_artist(artist)

    def set_album(self, album: str) -> None:
        self._song_data.set_album(album)

    def set_cover(self, url: str) -> None:
        self._picture.set_property("image", dowload_image(url))


class MediaButtons(Widget.Box):
    def __init__(self) -> None:
        play_btn = MediaButton("media-playback-start", _play)
        pause_btn = MediaButton("media-playback-pause", _pause)
        previous_btn = MediaButton("media-skip-backward", _previous)
        next_btn = MediaButton("media-skip-forward", _next)
        super().__init__(child=[play_btn, pause_btn, previous_btn, next_btn], vertical=False, spacing=32, halign="center")


def media() -> None:
    session_bus = dbus.SessionBus()
    spotify = session_bus.get_object("org.mpris.MediaPlayer2.spotify", "/org/mpris/MediaPlayer2")
    interface = dbus.Interface(spotify, "org.freedesktop.DBus.Properties")

    song_box = SongBox()
    Utils.Poll(1000, lambda _: _update_song_box(song_box, interface))

    media_buttons = MediaButtons()
    box = Widget.Box(child=[song_box, media_buttons], vertical=True, valign="center", halign="center", spacing=16)

    Widget.Window(
        popup=True,
        child=box,
        namespace="ignis_MEDIA",
        kb_mode="on_demand",
        visible=False,
        anchor=["top"],
        css_classes=["media-window"],
    )


def _update_song_box(song_box: SongBox, spotify_interface: dbus.Interface) -> None:
    metadata = spotify_interface.Get("org.mpris.MediaPlayer2.Player", "Metadata")
    song_title = metadata.get("xesam:title")
    art_url = metadata.get("mpris:artUrl")
    song_artist = metadata.get("xesam:artist")
    artists = ", ".join(song_artist)
    song_album = metadata.get("xesam:album")
    song_box.set_title(song_title)
    song_box.set_artist(artists)
    song_box.set_album(song_album)
    song_box.set_cover(art_url)


def _play(*args) -> None:
    Utils.exec_sh_async(
        "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play"
    )


def _pause(*args) -> None:
    Utils.exec_sh_async(
        "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Stop"
    )


def _previous(*args) -> None:
    Utils.exec_sh_async(
        "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"
    )


def _next(*args) -> None:
    Utils.exec_sh_async(
        "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"
    )
