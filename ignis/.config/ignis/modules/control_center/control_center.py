from ignis.widgets import Widget
from ignis.app import IgnisApp


app = IgnisApp.get_default()

def control_center():
    revealer = Widget.Revealer(
        transition_type="slide_left", transition_duration=300, reveal_child=True, child=Widget.Label(label="Control Center")
    )

    box = Widget.Box(
        child=[
            Widget.Button(
                vexpand=True,
                hexpand=True,
                css_classes=["unset"],
                on_click=lambda _: app.toggle_window("ignis_CONTROL_CENTER"),
            ),
            revealer
        ]
    )

    return Widget.RevealerWindow(
        visible=False,
        popup=True,
        kb_mode="on_demand",
        layer="top",
        css_classes=["unset"],
        anchor=["left", "right", "top", "bottom"],
        namespace="ignis_CONTROL_CENTER",
        child=box,
        revealer=revealer
    )
