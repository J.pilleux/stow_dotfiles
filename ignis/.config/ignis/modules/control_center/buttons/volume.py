from ignis.widgets import Widget
from ignis.services.audio import AudioService, Stream


audio = AudioService.get_default()


def device_entry(stream: Stream, _type: str):
    widget = Widget.Button(
        child=Widget.Box(
            child=[
                Widget.Icon(image="audio-card-symbolic"),
                Widget.Label(
                    label=stream.description,
                    ellipsize="end",
                    max_width_chars=30,
                    halign="start",
                    css_classes=["volume-entry-label"],
                ),
                Widget.Icon(
                    image="object-select-symbolic",
                    halign="end",
                    hexpand=True,
                    visible=stream.bind("is_default"),
                ),
            ]
        ),
        css_classes=["volume-entry", "unset"],
        hexpand=True,
        on_click=lambda x: setattr(audio, _type, stream),
    )
    stream.connect("removed", lambda x: widget.unparent())

    return widget


def _device_list(header_label: str, header_icon: str, _type: str, **kwargs):
    box = Widget.Box(
        css_classes=["unset"],
        vertical=True,
        child=[
            Widget.Box(
                child=[
                    Widget.Icon(image=header_icon, pixel_size=26),
                    Widget.Label(
                        label=header_label, halign="start", css_classes=["unset"]
                    ),
                ],
                css_classes=["unset"],
            ),
            Widget.Box(
                vertical=True,
                setup=lambda self: audio.connect(f"{_type}-added", lambda x, stream: self.append(device_entry(stream, _type))),
                )
        ],
    )


def volume_control():
    return
