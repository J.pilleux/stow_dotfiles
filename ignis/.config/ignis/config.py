from ignis.utils import Utils
from ignis.app import IgnisApp

from modules.powermenu import powermenu
from modules.media import media
from modules.control_center.control_center import control_center

app = IgnisApp.get_default()
app.apply_css(Utils.get_current_dir() + "/style.scss")

powermenu()
control_center()
media()
