# Sourcing common part
export EDITOR="nvim"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"

export MYSSHGIT="ssh://git@gitlab.com/J.pilleux"
export MYHTTPGET="https://gitlab.com/J.pilleux"
export TERM=xterm-256color

export GOPATH="${HOME}/go"

if [ -f "${XDG_CONFIG_HOME}/zsh/.zshrc" ]; then
    export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"
    # export ZSH="$HOME/.config/oh-my-zsh"
    export HISTFILE="${MY_CONFDIR}/zsh/.zsh_history"
fi

# include .bashrc if it exists
[[ -f "${ZDOTDIR}/.zshrc" ]] && source "${ZDOTDIR}/.zshrc"

# set PATH so it includes user's private bin if it exists
[[ -d "${HOME}/bin" ]] && PATH="${HOME}/bin:${PATH}"

# set PATH so it includes user's private bin if it exists
[[ -d "${HOME}/.local/bin" ]] && PATH="${HOME}/.local/bin:${PATH}"

# set PATH so it includes user's private bin if it exists
[[ -d "${HOME}/.local/usr_bin" ]] && PATH="${HOME}/.local/usr_bin:${PATH}"

# Start automatically ssh agent
if ! pgrep -u "${USER}" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "${XDG_RUNTIME_DIR}/ssh-agent.env"
fi
if [[ ! "${SSH_AUTH_SOCK}" ]]; then
    source "${XDG_RUNTIME_DIR}/ssh-agent.env" >/dev/null
fi

