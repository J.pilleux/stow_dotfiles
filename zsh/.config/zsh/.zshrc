# We can not use source_part
# Shell independent
# source_part "${XDG_CONFIG_HOME}/zsh/ohmyzsh_cfg.zsh"   # ohmyzsh config specific

HISTFILE="${XDG_CONFIG_HOME}/zsh/.histfile"
HISTSIZE=10000
SAVEHIST=10000
unsetopt beep notify

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/julien/.zshrc'

autoload -Uz compinit
compinit

# Mode vim
bindkey -v

# On remappe pour le bépo
bindkey -a c vi-backward-char
bindkey -a r vi-forward-char
bindkey -a é vi-forward-word
bindkey -a t vi-down-line-or-history
bindkey -a s vi-up-line-or-history
bindkey -a $ vi-end-of-line
bindkey -a 0 vi-digit-or-beginning-of-line
bindkey -a l vi-change
bindkey -a L vi-change-eol
bindkey -a dd vi-change-whole-line
bindkey -a h vi-replace-chars
bindkey -a H vi-replace
bindkey -a k vi-substitute

# Bind for the search through history like bash
bindkey '^R' history-incremental-search-backward
bindkey "^[[5~" history-beginning-search-backward
bindkey "^[[6~" history-beginning-search-forward

# Sourcing things
test -f "${XDG_CONFIG_HOME}/zsh/aliases.sh" && source "${XDG_CONFIG_HOME}/zsh/aliases.sh"
test -f "${XDG_CONFIG_HOME}/zsh/functions.sh" && source "${XDG_CONFIG_HOME}/zsh/functions.sh"
test -f "${XDG_CONFIG_HOME}/zsh/z.sh" && source "${XDG_CONFIG_HOME}/zsh/z.sh"
test -f "$HOME/.cargo/env" && source "$HOME/.cargo/env"
test -f "${HOME}/.override.sh" && source "${HOME}/.override.sh"

PROMPT="%F{green}%n%f@%F{blue}%m%f %F{red}%~%f $ "

