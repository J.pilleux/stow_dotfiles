# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
    imports = [
        # Include the results of the hardware scan.
        /etc/nixos/hardware-configuration.nix
    ];

    boot = {
        loader = {
            # Bootloader.
            systemd-boot = {
                enable = true;
                configurationLimit = 10;
            };
            efi.canTouchEfiVariables = true;
        };
        initrd.kernelModules = [ "amdgpu" ];
    };

    hardware = {
        pulseaudio.enable = false;
        opengl = {
            extraPackages = with pkgs; [
                rocmPackages.clr.icd
            ];
            driSupport = true; # This is already enabled by default
            driSupport32Bit = true; # For 32 bit applications
        };
        bluetooth = {
            enable = true;
            powerOnBoot = true;
        };
    };

    networking.hostName = "nixos"; # Define your hostname.
    # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Enable networking
    networking.networkmanager.enable = true;
    
    # Set your time zone.
    time.timeZone = "Europe/Paris";

    # Select internationalisation properties.
    i18n.defaultLocale = "en_US.UTF-8";

    i18n.extraLocaleSettings = {
        LC_ADDRESS = "fr_FR.UTF-8";
        LC_IDENTIFICATION = "fr_FR.UTF-8";
        LC_MEASUREMENT = "fr_FR.UTF-8";
        LC_MONETARY = "fr_FR.UTF-8";
        LC_NAME = "fr_FR.UTF-8";
        LC_NUMERIC = "fr_FR.UTF-8";
        LC_PAPER = "fr_FR.UTF-8";
        LC_TELEPHONE = "fr_FR.UTF-8";
        LC_TIME = "fr_FR.UTF-8";
    };

    services.pipewire = {
        enable = true;
        alsa.enable = true;
        pulse.enable = true;
    };

    services.flatpak.enable = true;

    services.blueman.enable = true;
    services.dbus.enable = true;
    xdg.portal = {
        enable = true;
        wlr.enable = true;
        # gtk portal needed to make gtk apps happy
        # extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };

    # Configure console keymap
    console.keyMap = "fr-bepo";

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.julien = {
        shell = pkgs.bash;
        isNormalUser = true;
        description = "julien";
        extraGroups = [ "networkmanager" "wheel" ];
        packages = with pkgs; [
            (wineWowPackages.full.override {
             wineRelease = "staging";
             mingwSupport = true;
             })
            winetricks
        ];
    };

    # Enable automatic login for the user.
    services.getty.autologinUser = "julien";

    # Allow unfree packages
    nixpkgs.config.allowUnfree = true;

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
        linuxKernel.packages.linux_6_1.xpadneo
        gnome.gnome-themes-extra
        cinnamon.nemo
        blueman
        bottles
        btop
        calcurse
        clinfo
        dbus
        discord
        dolphin
        dunst
        floorp
        gcc
        gimp
        git
        gnumake
        grimblast
        kitty
        mangohud
        lutris (
            lutris.override {
                extraPkgs = pkgs: [
                    pkgs.libnghttp2
                    pkgs.winetricks
                ];
            }
        )
        (catppuccin-gtk.override {
             accents = [ "mauve" ]; # You can specify multiple accents here to output multiple themes
             size = "standard";
             tweaks = [ "rimless" "black" ]; # You can also specify multiple tweaks here
             variant = "frappe";
         })
        neovim
        nodejs_21
        os-prober
        oversteer
        pavucontrol
        pfetch
        protontricks
        protonup
        radeontop
        ripgrep
        slurp
        spotify
        stow
        swaybg
        thunderbird
        tmux
        unzip
        waybar
        wine
        wine64
        wl-clipboard
        wofi
        xclip
        xdg-utils
        gamemode
        wget
        libyaml
    ];

    programs = {
        zsh.enable = true;
        hyprland = {
            enable = true;
            xwayland.enable = true;
        };
        steam = {
            enable = true;
            remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
        };
    };

    # List services that you want to enable:

    # Enable the OpenSSH daemon.
    # services.openssh.enable = true;

    # Open ports in the firewall.
    # networking.firewall.allowedTCPPorts = [ ... ];
    # networking.firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # networking.firewall.enable = false;

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "23.11"; # Did you read the comment?

    nix.settings.experimental-features = [ "nix-command" "flakes" ];
}
