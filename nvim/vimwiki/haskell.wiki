= Haskell =

= Folds =

folds works like reduces functions in other programming languages.

foldl is a left fold
foldl :: Foldable t => (a -> b -> a) -> a -> t b -> a

foldr is a right fold
foldr :: Foldable t => (a -> b -> b) -> b -> t a -> b

foldl1 and foldr1 are similar to foldl and foldr, but they don't require an initial value (they will deduce it).


scanl, scanr, scanl1 and scanr1 are similar but they report all intermediate results.

```haskell
sum' :: (Num a) => [a] -> a
sum' = scanl1 (+)

ghci> sum' [3, 5, 2, 1]
[3, 8, 10, 11]
```
