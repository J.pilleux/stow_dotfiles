{ pkgs ? import <nixpkgs> {} }:

  pkgs.mkShell {
    nativeBuildInputs = with pkgs.buildPackages; [ lua-language-server nixd stylua ];
}
