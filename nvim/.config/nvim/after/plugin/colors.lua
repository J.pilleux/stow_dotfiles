-- Removing bold and italic from catppuccin

vim.o.background = "dark"

local ok, catppuccin = pcall(require, "catppuccin")
if ok then
    catppuccin.setup({
        no_bold = true,
        no_italic = true
    })
end

function ColorMyPencils(color)
    color = color or "tokyonight-night"
    vim.cmd.colorscheme(color)

    -- Put the background in transparent
    vim.api.nvim_set_hl(0, "DiffAdd", { bg = "#000000" })
    vim.api.nvim_set_hl(0, "DiffText", { bg = "#FFFFFF" })
end

vim.api.nvim_set_hl(0, "GitConflictCurrent", { fg = "#c0caf5", bg = "#3d59a1" })
vim.api.nvim_set_hl(0, "GitConflictIncoming", { fg = "#c0caf5", bg = "#394b70" })
vim.api.nvim_set_hl(0, "GitConflictAncestor", { fg = "#c0caf5", bg = "#c53b53" })

vim.api.nvim_set_hl(0, "GitConflictCurrentLabel", { fg = "#1f2335", bg = "#7aa2f7" })
vim.api.nvim_set_hl(0, "GitConflictIncomingLabel", { fg = "#1f2335", bg = "#7aa2f7" })
vim.api.nvim_set_hl(0, "GitConflictAncestorLabel", { fg = "#1f2335", bg = "#7aa2f7" })

vim.api.nvim_set_hl(0, "Normal", { ctermbg = nil, bg = nil })

vim.cmd.colorscheme("tokyonight-moon")

vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "#a6adc8" })
vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "#a6adc8" })
vim.api.nvim_set_hl(0, "LineNr", { fg = "#89b4fa" })

vim.cmd [[
    hi! Normal ctermbg=NONE guibg=NONE
    hi! NonText ctermbg=NONE guibg=NONE
]]
