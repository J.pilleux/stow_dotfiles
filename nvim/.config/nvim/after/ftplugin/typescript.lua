vim.opt.shiftwidth = 2

vim.keymap.set("n", "<Leader>f", "<cmd>lua vim.lsp.buf.format()<CR>", { desc = "Format code (LSP)" })

