local utils = require("nvim_lua.utils")

-- Automatically source and re-compile packer whenever you save this init.lua
local invert_bool = function()
	vim.cmd(":norm! yiw")
	local yanked = vim.fn.getreg('"')
	if yanked == "True" then
		vim.cmd("norm! ciwFalse")
	elseif yanked == "False" then
		vim.cmd("norm! ciwTrue")
	elseif yanked == "true" then
		vim.cmd("norm! ciwfalse")
	elseif yanked == "false" then
		vim.cmd("norm! ciwtrue")
	end
end

local uppercase_and_bracket = function()
	local line = vim.api.nvim_get_current_line()
	local col = vim.api.nvim_win_get_cursor(0)[2]

	if col < #line then
		local char = line:sub(col + 1, col + 1):upper()
		local new_line = line:sub(1, col) .. "[" .. char .. "]" .. line:sub(col + 2)
		vim.api.nvim_set_current_line(new_line)
	end
end

local python_to_json = function ()
	vim.cmd("%s/'/\"/g")
	vim.cmd("%s/True/true/g")
	vim.cmd("%s/False/false/g")
	vim.cmd("%s/None/null/g)")
end

vim.api.nvim_create_user_command("InvertBool", invert_bool, {})
vim.api.nvim_create_user_command("UpperCaseAndBracket", uppercase_and_bracket, {})
vim.api.nvim_create_user_command("PythonToJson", python_to_json, {})

vim.api.nvim_create_autocmd("BufWritePost", {
	pattern = { "*.ml", "*.mli" },
	desc = "Rebuild the dune project each time a Ocaml file is saved",
	group = vim.api.nvim_create_augroup("rebuild-dune", { clear = true }),
	callback = function()
		-- vim.api.nvim_exec(":! dune build", true)
		vim.cmd([[:silent! dune build]])
	end,
})

vim.api.nvim_create_autocmd("TextYankPost", {
	desc = "Highlight when yanking text",
	group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
	callback = function()
		vim.highlight.on_yank()
	end,
})

vim.api.nvim_create_autocmd("BufEnter", {
	pattern = "*.h",
	group = vim.api.nvim_create_augroup("header_template", { clear = true }),
	callback = function()
		utils.use_skeleton("c_header.h")
	end,
})

vim.api.nvim_create_autocmd("BufEnter", {
	pattern = { "*.bash", "*.sh" },
	group = vim.api.nvim_create_augroup("bash_template", { clear = true }),
	callback = function()
		utils.use_skeleton("bash.sh")
	end,
})
