-- {W} -> [É]
-- ——————————
-- On remappe W sur É :
vim.keymap.set("", 'é', 'w')
vim.keymap.set("", 'É', 'W')

-- Corollaire: on remplace les text objects aw, aW, iw et iW
-- pour effacer/remplacer un mot quand on n’est pas au début (daé / laé).
vim.keymap.set("o", 'aé', 'aw')
vim.keymap.set("o", 'aÉ', 'aW')
vim.keymap.set("o", 'ié', 'iw')
vim.keymap.set("o", 'iÉ', 'iW')

vim.keymap.set("", '<C-,>', '@@')

-- Pour faciliter les manipulations de fenêtres, on utilise {W} comme un Ctrl+W :
vim.keymap.set("", 'w', '<C-w>')
vim.keymap.set("", 'W', '<C-w><C-w>')

-- Accès direct sur les chiffres
vim.keymap.set("", '"', '1')
vim.keymap.set("", '1', '"')

vim.keymap.set("", '«', '2')
vim.keymap.set("", '2', '«')

vim.keymap.set("", '»', '3')
vim.keymap.set("", '3', '»')

vim.keymap.set("", '(', '4')
vim.keymap.set("", '4', '(')

vim.keymap.set("", ')', '5')
vim.keymap.set("", '5', ')')

vim.keymap.set("", '@', '6')
vim.keymap.set("", '6', '@')

vim.keymap.set("", '+', '7')
vim.keymap.set("", '7', '+')

vim.keymap.set("", '-', '8')
vim.keymap.set("", '8', '-')

vim.keymap.set("", '/', '9')
vim.keymap.set("", '9', '/')

vim.keymap.set("", '*', '0')
vim.keymap.set("", '0', '*')

-- [HJKL] -> {CTSR}
-- ————————————————
-- {cr} = « gauche / droite »
vim.keymap.set("", 'c', 'h', { noremap = true })
vim.keymap.set("", 'r', 'l', { noremap = true })

-- {ts} = « haut / bas »
vim.keymap.set("", 't', 'j', { noremap = true })
vim.keymap.set("", 's', 'k', { noremap = true })

-- {CR} = « haut / bas de l'écran »
vim.keymap.set("", 'C', 'H', { noremap = true })
vim.keymap.set("", 'R', 'L', { noremap = true })

-- {TS} = « joindre / aide »
vim.keymap.set("", 'T', 'J', { noremap = true })
vim.keymap.set("", 'S', 'K', { noremap = true })

-- {HJKL} <- [CTSR]
-- ————————————————
-- {J} = « Jusqu'à »            (j = suivant, J = précédant)
vim.keymap.set("", 'j', 't', { noremap = true })
vim.keymap.set("", 'J', 'T', { noremap = true })

-- {L} = « Change »             (l = attend un mvt, L = jusqu'à la fin de ligne)
vim.keymap.set("", 'l', 'c', { noremap = true })
vim.keymap.set("", 'L', 'C', { noremap = true })

-- {H} = « Remplace »           (h = un caractère slt, H = reste en « Remplace »)
vim.keymap.set("", 'h', 'r', { noremap = true })
vim.keymap.set("", 'H', 'R', { noremap = true })

-- {K} = « Substitue »          (k = caractère, K = ligne)
vim.keymap.set("", 'k', 's', { noremap = true })
-- vim.keymap.set("", 'K', 'S', { noremap = true })

-- Corollaire: correction orthographique
vim.keymap.set("", ']k', ']s')
vim.keymap.set("", ']K', ']S')

-- Accès direct pour les [ et ]
vim.keymap.set("", '<C-]>', '<C-)>')
vim.keymap.set("", '<C-[>', '<C-(>')

-- Désambiguation de {g}
-- —————————————————————
-- ligne écran précédente / suivante (à l'intérieur d'une phrase)
vim.keymap.set("", 'gs', 'gk')
vim.keymap.set("", 'gs', 'gj')

-- onglet précédent / suivant
vim.keymap.set("", 'gb', 'gT', { desc = "Go to previous tab" })
vim.keymap.set("", 'gé', 'gt', { desc = "Go to next tab" })

-- optionnel : {gB} / {gÉ} pour aller au premier / dernier onglet
-- ————————————————————————
vim.keymap.set("", 'gB', ':exe "silent! tabfirst"<CR>', { desc = "Go to first tab" })
vim.keymap.set("", 'gÉ', ':exe "silent! tablast"<CR>', { desc = "Go to last tab" })

--optionnel : {g"} pour aller au début de la ligne écran
vim.keymap.set("", 'g"', 'g0')

-- Remap de la recherche sur * et #
-- ————————————————————————————————
vim.keymap.set("", 'ç', '*')
vim.keymap.set("", 'Ç', '#')

-- Permutation pour la recherche
-- ————————————————————————
vim.keymap.set("", ',', ';')
vim.keymap.set("", ';', ',')
vim.keymap.set("", 'g,', 'g;')
vim.keymap.set("", 'g;', 'g,')

-- Dans le terminal
-- ————————————————————————
vim.keymap.set("t", '<C-c>', '<C-\\><C-N><C-w>c')
vim.keymap.set("t", '<C-t>', '<C-\\><C-N><C-w>t')
vim.keymap.set("t", '<C-s>', '<C-\\><C-N><C-w>s')
vim.keymap.set("t", '<C-r>', '<C-\\><C-N><C-w>r')

vim.keymap.set("i", "<C-é>", "<C-w>")

-- Custom keybinds
-- ————————————————————————
vim.keymap.set("i", "<C-C>", "<ESC>")

-- Move selection
vim.keymap.set("v", "T", ":m '>+1<CR>gv=gv", { desc = "[T] Move selection up" })
vim.keymap.set("v", "S", ":m '<-2<CR>gv=gv", { desc = "[S] Move selection down" })

-- Keep cursor in place when J
vim.keymap.set("n", "T", "mzJ`z")

-- Center cursor when C-d and C-u
vim.keymap.set("n", "<C-d>", "<C-d>zz", { desc = "[D]own half page" })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { desc = "[U]p half page" })

vim.keymap.set("n", "n", "nzzzv", { desc = "[N]ext search occurence" })
vim.keymap.set("n", "N", "Nzzzv", { desc = "[N] Previous search occurence" })

vim.keymap.set("x", "<Leader>p", "\"_dP", { desc = "[P]aste" })

vim.keymap.set("n", "<Leader>y", "\"+y", { desc = "[Y]ank to System clip" })
vim.keymap.set("v", "<Leader>y", "\"+y", { desc = "[Y]ank to System clip" })
vim.keymap.set("n", "<Leader>Y", "ggyG", { desc = "[Y]ank the whole file" })

vim.keymap.set("t", "<Esc><Esc>", "<C-\\><C-n>")

vim.keymap.set("n", "<A-n>", "<CMD>cnext<CR>", { desc = "[N]ext queckfix" })
vim.keymap.set("n", "<A-p>", "<CMD>cprev<CR>", { desc = "[P]revious queckfix" })

vim.keymap.set("i", "<S-CR>", "<C-o>o")

vim.keymap.set("n", "<Leader>mr", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>",
	{ desc = "[R]eplace current word" })
vim.keymap.set("n", "Q", "@", { desc = "Run macro" })

-- Tmux navigation
vim.keymap.set("n", "<M-c>", ":lua require('nvim-tmux-navigation').NvimTmuxNavigateLeft() <CR>",
	{ desc = "[M-c] Move right Tmux / nvim pane" })
vim.keymap.set("n", "<M-t>", ":lua require('nvim-tmux-navigation').NvimTmuxNavigateDown() <CR>",
	{ desc = "[M-t] Move bottom Tmux / nvim pane" })
vim.keymap.set("n", "<M-s>", ":lua require('nvim-tmux-navigation').NvimTmuxNavigateUp() <CR>",
	{ desc = "[M-s] Move top Tmux / nvim pane" })
vim.keymap.set("n", "<M-r>", ":lua require('nvim-tmux-navigation').NvimTmuxNavigateRight() <CR>",
	{ desc = "[M-r] Move left Tmux / nvim pane" })

vim.keymap.set("v", "<Leader>q", function()
	vim.cmd('normal! gv"vy')
	local raw = vim.fn.getreg("v")
	vim.fn.setreg("v", old_reg)
	print(raw)
	vim.api.nvim_feedkeys(":%s/" .. raw .. "/", "n", false)
end, { desc = "[Q]uery replace" })

-- File saving
vim.keymap.set("n", "<C-s>", ":w <CR>", { desc = "[S]ave current file" })
vim.keymap.set("n", "<C-S>", ":wa <CR>", { desc = "[S]ave all files" })
vim.keymap.set("i", "<C-s>", "<C-O>:w <CR>", { desc = "[S]ave current file" })
vim.keymap.set("i", "<C-S>", "<C-O>:wa <CR>", { desc = "[S]ave all files" })

-- Coding haxxx
vim.keymap.set("i", "<A-,>", "<C-O>A;", { desc = "[A-,] Append semicolon to current line" })
vim.keymap.set("n", "<A-,>", "A;<Esc>", { desc = "[A-,] Append semicolon to current line" })
vim.keymap.set("n", "<A-d>", "Vyp", { desc = "[A-d] Duplicate current line" })

-- Open things
vim.keymap.set("n", "<leader>oe", ":Ex<CR>", { desc = "Open file [E]xplorer" })
vim.keymap.set("n", "<leader>om", ":sp<CR><C-W><C-W><CMD>terminal<CR>i", { desc = "Open ter[M]inal" })
vim.keymap.set("n", "<Leader>-", ": sp <CR>", { desc = "[-] Horizontal split" })
vim.keymap.set("n", "<Leader>|", ": vsp <CR>", { desc = "[|] Vertical split" })

-- Misc
vim.keymap.set("n", "<leader>mb", ":InvertBool<CR>", { desc = "Invert [B]oolean" })
vim.keymap.set("n", "<Leader>mx", ":!chmod +x %<CR>", { desc = "Making file e[X]ecutable" })
vim.keymap.set("n", "<Leader>mu", "<CMD> UpperCaseAndBracket <CR>", { desc = "Making file e[X]ecutable" })
vim.keymap.set("n", "<Leader><Leader>x", "<cmd>source %<CR>", { desc = "Sourcing the current file" })

vim.keymap.set("n", "<C-m>", "<CMD> only <CR>", { desc = "Close all [O]ther slpits" })

-- quickfix list
vim.keymap.set("n", "<Leader><Leader>n", "<CMD> cnext <CR>", { desc = "[N]ext quickfix" })
vim.keymap.set("n", "<Leader><Leader>p", "<CMD> cprev <CR>", { desc = "[P]revious quickfix" })
