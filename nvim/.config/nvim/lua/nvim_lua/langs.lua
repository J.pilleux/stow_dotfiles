local M = {
    all_dev_filetype = { "python", "javascript", "typescript", "typescriptreact", "react", "go", "haskell", "ocaml", "lua" }
}

return M
