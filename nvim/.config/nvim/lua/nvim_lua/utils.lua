local M = {}

M.split_string = function(str, sep)
    local t = {}
    for s in string.gmatch(str, "([^" .. sep .. "]+)") do
        table.insert(t, s)
    end
    return t
end

M.insert_text = function(text)
    local pos = vim.api.nvim_win_get_cursor(0)[2]
    local line = vim.api.nvim_get_current_line()
    local nline = line:sub(0, pos) .. text .. line:sub(pos + 1)
    vim.api.nvim_set_current_line(nline)
    local text_len = pos + 1 + string.len(text)
    vim.api.nvim_win_set_cursor(0, { vim.api.nvim_win_get_cursor(0)[1], text_len })
    vim.cmd(":norm! a")
end

local function is_current_buf_empty()
    return vim.api.nvim_buf_line_count(0) == 1 and vim.api.nvim_buf_get_lines(0, 0, -1, false)[1]
end

local function file_exists(fname)
    local file = io.open(fname, "r")
    if file then
        io.close(file)
        return true
    else
        return false
    end
end

M.file_exists = file_exists

M.use_skeleton = function(skeleton_name)
    if not is_current_buf_empty() then
        return
    end

    local skeleton_file = os.getenv("HOME") .. "/.config/nvim/templates/" .. skeleton_name
    if not file_exists(skeleton_file) then
        print("File " .. skeleton_file .. " does not exist")
        return
    end

    vim.cmd("0r " .. skeleton_file)
end

M.list_files_in_dir = function(directory_path)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls "' .. vim.fn.expand(directory_path) .. '"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

M.get_file_content = function(filepath)
    return vim.fn.readfile(filepath)
end

return M
