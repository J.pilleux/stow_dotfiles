local M = {}

local function insert_template(opts)
	local template_dir = "~/.config/nvim/templates/"
	local templates = utils.list_files_in_dir(template_dir)
	opts = opts or {}
	pickers
		.new(opts, {
			prompt_title = "File templates",
			finder = finders.new_table({
				results = templates
			}),
			attach_mappings = function(prompt_bufnr, map)
				actions.select_default:replace(function()
					actions.close(prompt_bufnr)
					local selection = action_state.get_selected_entry()
					utils.use_skeleton(selection[1])
				end)
				return true
			end,
			sorter = conf.generic_sorter(opts),
			previewer = previewers.new_buffer_previewer {
				title = "Template preview",
				define_preview = function(self, entry, status)
					local content = vim.fn.readfile(vim.fn.expand(template_dir) .. entry[1])
					vim.api.nvim_buf_set_lines(self.state.bufnr, 0, -1, false, content)
				end
			}
		})
		:find()
end

M.setup = function()
	vim.keymap.set("n", "<leader>st", insert_template, { desc = "[S]earch [T]emplate" })
end


return M
