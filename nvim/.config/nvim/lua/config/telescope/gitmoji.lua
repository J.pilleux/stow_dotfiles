local M = {}

local function gitmoji(opts)
	opts = opts or {}
	local emojis = vim.api.nvim_exec(":! gitmoji -l", true)
	local t = utils.split_string(emojis, "\n")

	pickers
		.new(opts, {
			prompt_title = "Gitmoji",
			finder = finders.new_table({
				results = t,
			}),
			attach_mappings = function(prompt_bufnr, map)
				actions.select_default:replace(function()
					actions.close(prompt_bufnr)
					local selection = action_state.get_selected_entry()
					local emoji = string.sub(selection[1], 1, 4)
					utils.insert_text(emoji)
				end)
				return true
			end,
			sorter = conf.generic_sorter(opts),
		})
		:find()
end

M.setup = function()
	vim.keymap.set("n", "<leader>sj", gitmoji, { desc = "[S]earch available gitmo[J]i" })
end

return M
