local M = {}

local function git_cmds(opts)
	opts = opts or {}
	pickers
		.new(opts, {
			prompt_title = "colors",
			finder = finders.new_table({
				results = { "Git pull", "Git rebase --continue", "Git pull", "Git push" },
			}),
			attach_mappings = function(prompt_bufnr, map)
				actions.select_default:replace(function()
					actions.close(prompt_bufnr)
					local selection = action_state.get_selected_entry()
					vim.api.nvim_command(selection[1])
				end)
				return true
			end,
			sorter = conf.generic_sorter(opts),
		})
		:find()
end

M.setup = function()
	vim.keymap.set("n", "<leader>sgm", git_cmds, { desc = "[S]earch [G]it co[M]mands" })
end

return M
