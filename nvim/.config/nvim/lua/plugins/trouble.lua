return {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = {
        action_keys = {
            previous = "s",
            next = "t",
            switch_severity = "u",
            refresh = "r"
        }
    },
}
