return {
	"akinsho/git-conflict.nvim",
	version = "*",
	config = function()
		require("git-conflict").setup({
			default_mappings = false, -- disable buffer local mapping created by this plugin
			default_commands = true, -- disable commands created by this plugin
			disable_diagnostics = false, -- This will disable the diagnostics in a buffer whilst it is conflicted
			list_opener = "copen", -- command or function to open the conflicts list
			highlights = { -- They must have background color, otherwise the default color will be used
				incoming = "DiffAdd",
				current = "DiffText",
			},
		})

		vim.keymap.set("n", "<Leader>gco", "<CMD>GitConflictChooseOurs<CR>", { desc = "Choose [O]urs" })
		vim.keymap.set("n", "<Leader>gct", "<CMD>GitConflictChooseTheirs<CR>", { desc = "Choose [T]heirs" })
		vim.keymap.set("n", "<Leader>gcb", "<CMD>GitConflictChooseBoth<CR>", { desc = "Choose [B]oth" })
		vim.keymap.set("n", "<Leader>gcn", "<CMD>GitConflictChooseNone<CR>", { desc = "Choose [N]one" })

		vim.keymap.set("n", "<Leader>gcq", "<CMD>GitConflictListQf<CR>", { desc = "[Q]uickfix list" })

		vim.keymap.set("n", "<Leader>gce", "<CMD>GitConflictNextConflict<CR>", { desc = "N[E]xt conflict" })
		vim.keymap.set("n", "<Leader>gcp", "<CMD>GitConflictPrevConflict<CR>", { desc = "[P]revious conflict" })
	end,
}
