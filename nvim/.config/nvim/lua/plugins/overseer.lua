return {
    "stevearc/overseer.nvim",
    opts = {},
    keys = {
        { "<leader>oo", ":OverseerToggle<CR>", desc = "[O]pen [O]verseer" },
        { "<leader>or", ":OverseerRun<CR>", desc = "[O]pen Overseer [R]unner" }
    }
}
