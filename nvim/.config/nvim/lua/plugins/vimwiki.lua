return {
    "vimwiki/vimwiki",
    config = function() 
        vim.keymap.set("n", "<leader>ow", ":VimwikiIndex<CR>", { desc = "Open Vim[W]iki" })
    end,
}
