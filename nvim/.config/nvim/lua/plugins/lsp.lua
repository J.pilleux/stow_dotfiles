return {
    {
        "williamboman/mason.nvim",
        lazy = false,
        config = function()
            require("mason").setup()
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        lazy = false,
        opts = {
            auto_install = true,
        },
    },
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            "saghen/blink.cmp",
            "L3MON4D3/LuaSnip",
            {
                "folke/lazydev.nvim",
                ft = "lua",
                opts = {
                    library = {
                        { path = "${3rd}/luv/library", words = { "vim%.uv" } },
                    },
                },
            }
        },
        opts = {
            servers = {
                lua_ls = {},
                eslint = {},
                ts_ls = {},
                cssls = {},
                jsonls = {},
                bashls = {},
                gopls = {},
                rust_analyzer = {},
                ruff = {},
                pyright = {},
                hls = {}
            }
        },
        config = function(_, opts)
            require("luasnip.loaders.from_snipmate").lazy_load()
            local lspconfig = require("lspconfig")

            for server, config in pairs(opts.servers) do
              config.capabilities = require('blink.cmp').get_lsp_capabilities(config.capabilities)
              lspconfig[server].setup(config)
            end

            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup("UserLspConfig", {}),
                callback = function(ev)
                    vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"
                    local opts = function(desc)
                        return { buffer = ev.buf, remap = false, desc = desc }
                    end

                    local builtin = require("telescope.builtin")

                    -- Code navigation fast
                    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts("[G]o to [D]efinition"))
                    vim.keymap.set("n", "<leader>.", vim.lsp.buf.references, opts("[G]o to [R]eferences"))
                    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts("[G]o to [I]mplementation"))
                    vim.keymap.set("n", "gc", vim.lsp.buf.declaration, opts("[G]o to de[C]laration"))

                    -- Code navigation slow ?
                    vim.keymap.set("n", "<leader>lgd", vim.lsp.buf.definition, opts("[G]o to [D]efinition"))
                    vim.keymap.set("n", "<leader>lgr", builtin.lsp_references, opts("[G]o to [R]eferences"))
                    vim.keymap.set("n", "<leader>lgi", vim.lsp.buf.implementation, opts("[G]o to [I]mplementation"))
                    vim.keymap.set("n", "<leader>lgc", vim.lsp.buf.declaration, opts("[G]o to de[C]laration"))
                    vim.keymap.set("n", "<leader>lgn", function() vim.diagnostic.jump { count = 1 } end,
                        opts("[G]o to [N]ext diagnostic"))
                    vim.keymap.set("n", "<leader>lgp", function() vim.diagnostic.jump { count = -1 } end,
                        opts("[G]o to [P]revious diagnostic"))

                    vim.keymap.set("n", "<Leader>lr", vim.lsp.buf.rename, opts("[R]ename"))
                    vim.keymap.set("n", "<Leader>ld", vim.diagnostic.open_float, opts("Open [D]iagnostics"))
                    vim.keymap.set("n", "<Leader>lt", vim.lsp.buf.type_definition, opts("[T]ype definition"))
                    vim.keymap.set("n", "<Leader>lo", builtin.lsp_document_symbols, opts("D[O]cument symbols"))
                    vim.keymap.set(
                        "n",
                        "<Leader>lws",
                        builtin.lsp_dynamic_workspace_symbols,
                        opts("[W]orkspace symbols")
                    )
                    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts("Hover documentation"))
                    vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts("Signature documentation"))
                    vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, opts("[F]ormat file"))

                    vim.keymap.set("n", "<Leader>a", vim.lsp.buf.code_action, opts("Code [A]ction"))
                    vim.keymap.set("n", "<Leader>f", vim.lsp.buf.format, opts("[F]ormat document"))

                    -- What the point of these ?
                    vim.keymap.set("n", "<Leader>lwa", vim.lsp.buf.add_workspace_folder, opts("Workspace [A]dd folder"))
                    vim.keymap.set("n", "<Leader>lwr", vim.lsp.buf.remove_workspace_folder,
                        opts("Workspace [R]emove folder"))

                    vim.keymap.set("n", "<Leader>lS", "<CMD> LspStart <CR>", { desc = "Lsp [S]tart" })
                    vim.keymap.set("n", "<Leader>lT", "<CMD> LspStop <CR>", { desc = "Lsp s[T]op" })
                    vim.keymap.set("n", "<Leader>lR", "<CMD> LspRestart <CR>", { desc = "Lsp [R]estart" })

                    vim.keymap.set("n", "<Leader>ll", function()
                        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
                    end, opts("Workspace [L]ist folders"))
                end,
            })

            vim.diagnostic.config({
                virtual_text = true,
                signs = true,
                underline = true,
                severity_sort = false,
                float = true,
            })
        end,
    },
}
