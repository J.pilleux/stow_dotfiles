return {
    "lewis6991/gitsigns.nvim",
    opts = {
        signs = {
            add = { text = '+' },
            change = { text = '~' },
            delete = { text = '_' },
            topdelete = { text = '‾' },
            changedelete = { text = '~' },
        },
        on_attach = function(bufnr)
            local gs = package.loaded.gitsigns

            local function map(mode, l, r, opts)
                opts = opts or {}
                opts.buffer = bufnr
                vim.keymap.set(mode, l, r, opts)
            end

            -- Navigation
            map('n', '<leader>gbl', function() gs.blame_line { full = true } end, { desc = "Blame [L]ine" })
            map('n', '<leader>gbi', gs.toggle_current_line_blame, { desc = "[I]nline blame" })

            map({ 'n', 'v' }, '<leader>ghs', ':Gitsigns stage_hunk<CR>', { desc = "[S]tage hunk" })
            map({ 'n', 'v' }, '<leader>ghr', ':Gitsigns reset_hunk<CR>', { desc = "[R]eset hunk" })
            map('n', '<leader>ghu', gs.undo_stage_hunk, { desc = "[U]ndo stage hunk" })
            map('n', '<leader>ghp', gs.preview_hunk, { desc = "[P]review hunk" })
            map('n', '<leader>ghn', gs.next_hunk, { desc = "[N]ext hunk" })

            map('n', '<leader>gus', gs.stage_buffer, { desc = "B[U]ffer stage" })
            map('n', '<leader>gur', gs.reset_buffer, { desc = "B[U]ffer reset" })
            map('n', '<leader>gud', gs.diffthis, { desc = "B[U]ffer diff" })
            map('n', '<leader>ght', function() gs.diffthis('~') end, { desc = "B[U]ffer diff ~" })

            map('n', '<leader>gt', gs.toggle_deleted, { desc = "[T]oggle deleted" })
        end
    }
}
