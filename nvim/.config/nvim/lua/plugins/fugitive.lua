return {
    "tpope/vim-fugitive",
    keys = {
        { "<Leader>gs", vim.cmd.Git, desc = "[S]tatus" },
        { "<Leader>ge", ":Gedit<CR>", desc = "[E]dit" },
        { "<Leader>gE", ":Gedit :0<CR>", desc = "[E]edit mais pas pareil" },
        { "<Leader>gp", ":Git pull<CR>", desc = "[P]ull" },
        { "<Leader>gP", ":Git push<CR>", desc = "[P]ush" },
        { "<Leader>gm", ":Gmerge<CR>", desc = "[M]erge" },

        { "<Leader>gbf", ":Git blame<CR>", desc = "[B]lame file" },

        { "<Leader>gfd", ":GDelete<CR>", desc = "[F]ile [D]elete" },
        { "<Leader>gfm", ":GMove<CR>", desc = "[F]ile [M]ove" },

        { "<Leader>gll", ":Git log<CR>", desc = "[L]og" },
        { "<Leader>glo", ":Git log --oneline<CR>", desc = "[L]og --[O]neline" },

        { "<Leader>grr", ":Git rebase<CR>", desc = "[R]ebase" },
        { "<Leader>grc", ":Git rebase --continue<CR>", desc = "[R]ebase --[C]ontinue" },
        { "<Leader>ga", ":Git commit --amend<CR>", desc = "Commit --[A]mend" },

        -- Conflict solving
        {"n", "<Leader>gd", ":Gvdiffsplit!<CR>", desc = "[D]iff vsplit" },
        {"n", "<C-Left>", ":diffget //2<CR>", desc = "[T] Get left" },
        {"n", "<C-Right>", ":diffget //3<CR>", desc = "[N] Get right" },
    },
}
