return {
    'brooth/far.vim',
    config = function()
        vim.cmd [[ let g:far#enable_undo=1 ]]
    end
}
