return {
	{
		"CopilotC-Nvim/CopilotChat.nvim",
		branch = "main",
		dependencies = {
			{ "zbirenbaum/copilot.lua" }, -- or github/copilot.vim
			{ "nvim-lua/plenary.nvim" }, -- for curl, log wrapper
		},
		build = "make tiktoken", -- Only on MacOS or Linux
		config = function()
			require("CopilotChat").setup({
				debug = true,
				chat_autocomplete = true,
				mappings = {
					complete = {
						insert = "",
					},
				},
				prompts = {
					Tests = {
						prompt = '> /COPILOT_GENERATE\n\nPlease generate tests for my code. If it is python, please add some type hints and use pytest and pytest_mock.'
					}
				}
			})

			-- Normal mode
			vim.keymap.set("n", "<leader>cc", "<CMD> CopilotChatToggle <CR>", { desc = "Toggle [C]hat pane" })
			vim.keymap.set("n", "<leader>cs", "<CMD> CopilotChatStop <CR>", { desc = "[S]top copilot output" })
			vim.keymap.set("n", "<leader>cr", "<CMD> CopilotChatReset <CR>", { desc = "[R]eset chat window" })
			vim.keymap.set("n", "<leader>cu", "<CMD> CopilotChatDebugInfo <CR>", { desc = "[D]ebug information" })
			vim.keymap.set("n", "<leader>cm", "<CMD> CopilotChatModel <CR>", { desc = "Show chat available [M]odels" })
			vim.keymap.set("n", "<leader>ct", "<CMD> CopilotChatTests <CR>", { desc = "Write [T]ests for code" })
			vim.keymap.set("n", "<leader>cf", "<CMD> CopilotChatFixDiagnostic <CR>", { desc = "[F]ix diagnostics" })
			vim.keymap.set("n", "<leader>ci", "<CMD> CopilotChatCommit <CR>", { desc = "Write comm[I]t message" })
			vim.keymap.set("n", "<leader>cd", "<CMD> CopilotChatDocs <CR>", { desc = "Write [D]ocs for function" })
			vim.keymap.set(
				"n",
				"<leader>cI",
				"<CMD> CopilotChatCommitStaged <CR>",
				{ desc = "Write staged comm[I]t message" }
			)

			-- Visual mode
			vim.keymap.set("v", "<leader>ce", "<CMD> CopilotChatExplain <CR>", { desc = "[E]xplain selected code" })
			vim.keymap.set("v", "<leader>cr", "<CMD> CopilotChatReview <CR>", { desc = "[R]eview selected code" })
			vim.keymap.set("v", "<leader>co", "<CMD> CopilotChatOptimize <CR>", { desc = "[O]ptimize selected code" })
			vim.keymap.set("v", "<leader>cd", "<CMD> CopilotChatDocs <CR>", { desc = "[D]ocuments selected code" })
			vim.keymap.set(
				"v",
				"<leader>ct",
				"<CMD> CopilotChatTests <CR>",
				{ desc = "write [T]ests for selected code" }
			)
			vim.keymap.set("v", "<leader>cf", "<CMD> CopilotChatFixDiagnostic <CR>", { desc = "[F]ix diagnostics" })
		end,
	},
}
