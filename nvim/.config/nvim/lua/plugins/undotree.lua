return {
    'mbbill/undotree',
    keys = {
        { "<leader>ou", ":UndotreeToggle<CR>", desc = "Open [U]ndo tree" }
    }
}
