return {
	{
		"catppuccin/nvim",
		name = "catppuccin",
		lazy = false,
		priority = 1000,
	},
	{
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
		opts = {},
	},
	{
		"sonph/onehalf",
		lazy = false,
		priority = 1000,
	},
	{
		"cocopon/iceberg.vim",
		lazy = false,
		priority = 1000,
	},
	{
		"FrenzyExists/aquarium-vim",
		lazy = false,
		priority = 1000,
	},
	{
		"neanias/everforest-nvim",
		version = false,
		lazy = false,
		priority = 1000, -- make sure to load this before all the other start plugins
		-- Optional; default configuration will be used if setup isn't called.
		config = function()
			require("everforest").setup({
				-- Your config here
			})
		end,
	},
}
