return {
    "nvim-pack/nvim-spectre",
    config = function()
        require("spectre").setup({
            mapping = {
                ["toggle_line"] = {
                    map = "dd",
                    cmd = ":lua require('spectre').toggle_line()<CR>",
                    desc = "toggle item",
                },
                ["enter_file"] = {
                    map = "<cr>",
                    cmd = ":lua require('spectre.actions').select_entry()<CR>",
                    desc = "Open file",
                },
                ["send_to_qf"] = {
                    map = "<leader>rq",
                    cmd = ":lua require('spectre.actions').send_to_qf()<CR>",
                    desc = "Send [R]elpace items to [Q]uickfix",
                },
                ["replace_cmd"] = {
                    map = "<leader>rc",
                    cmd = ":lua require('spectre.actions').replace_cmd()<CR>",
                    desc = "[R]eplace [C]ommand",
                },
                ["show_option_menu"] = {
                    map = "<leader>ro",
                    cmd = ":lua require('spectre').show_options()<CR>",
                    desc = "Show [R]eplace [O]ptions",
                },
                ["run_current_replace"] = {
                    map = "<leader>rc",
                    cmd = ":lua require('spectre.actions').run_current_replace()<CR>",
                    desc = "[R]eplace [C]urrent line",
                },
                ["run_replace"] = {
                    map = "<leader>rr",
                    cmd = ":lua require('spectre.actions').run_replace()<CR>",
                    desc = "[R][R]eplace all",
                },
                ["change_view_mode"] = {
                    map = "<leader>rv",
                    cmd = ":lua require('spectre').change_view()<CR>",
                    desc = "Change [R]esult [V]iew mode",
                },
                ["change_replace_sed"] = {
                    map = "<leader>rs",
                    cmd = ":lua require('spectre').change_engine_replace('sed')<CR>",
                    desc = "Use sed to [R]eplace [S]",
                },
                ["change_replace_oxi"] = {
                    map = "<leader>rx",
                    cmd = ":lua require('spectre').change_engine_replace('oxi')<CR>",
                    desc = "[R] Use o[X]i to replace",
                },
                ["toggle_live_update"] = {
                    map = "<leader>ru",
                    cmd = ":lua require('spectre').toggle_live_update()<CR>",
                    desc = "[R][U]pdate when vim writes to file",
                },
                ["toggle_ignore_case"] = {
                    map = "<leader>ri",
                    cmd = ":lua require('spectre').change_options('ignore-case')<CR>",
                    desc = "[R] Toggle [I]gnore case",
                },
                ["toggle_ignore_hidden"] = {
                    map = "<leader>rh",
                    cmd = ":lua require('spectre').change_options('hidden')<CR>",
                    desc = "[R] Toggle search [H]idden",
                },
                ["resume_last_search"] = {
                    map = "<leader>rl",
                    cmd = ":lua require('spectre').resume_last_search()<CR>",
                    desc = "[R] repeat [L]ast search",
                },
            },
        })

        vim.keymap.set("n", "<leader>os", ":Spectre<CR>", { desc = "[O]pen [S]pectre"})
    end,
}
