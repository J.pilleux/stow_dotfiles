return { -- Collection of various small independent plugins/modules
    "echasnovski/mini.nvim",
    branch = "stable",
    config = function()
        -- Better Around/Inside textobjects
        --
        -- Examples:
        --  - va)  - [V]isually select [A]round [)]paren
        --  - yinq - [Y]ank [I]nside [N]ext [']quote
        --  - ci'  - [C]hange [I]nside [']quote
        require("mini.ai").setup({ n_lines = 500 })

        -- Add/delete/replace surroundings (brackets, quotes, etc.)
        --
        -- - saiw) - [S]urround [A]dd [I]nner [W]ord [)]Paren
        -- - sd'   - [S]urround [D]elete [']quotes
        -- - sr)'  - [S]urround [R]eplace [)] [']
        require("mini.cursorword").setup()

        require("mini.diff").setup {}
        local hipatterns = require("mini.hipatterns")
        hipatterns.setup {
            -- Table with highlighters (see |MiniHipatterns.config| for more details).
            -- Nothing is defined by default. Add manually for visible effect.
            highlighters = {
                -- Highlight standalone 'FIXME', 'HACK', 'TODO', 'NOTE'
                fixme     = { pattern = '%f[%w]()FIXME()%f[%W]', group = 'MiniHipatternsFixme' },
                hack      = { pattern = '%f[%w]()HACK()%f[%W]', group = 'MiniHipatternsHack' },
                todo      = { pattern = '%f[%w]()TODO()%f[%W]', group = 'MiniHipatternsTodo' },
                note      = { pattern = '%f[%w]()NOTE()%f[%W]', group = 'MiniHipatternsNote' },

                -- Highlight hex color strings (`#rrggbb`) using that color
                hex_color = hipatterns.gen_highlighter.hex_color(),
            },

            -- Delays (in ms) defining asynchronous highlighting process
            delay = {
                -- How much to wait for update after every text change
                text_change = 200,

                -- How much to wait for update after window scroll
                scroll = 50,
            },
        }
        require("mini.icons").setup {}

        require("mini.surround").setup {
            mappings = {
                add = 'ma',            -- Add surrounding in Normal and Visual modes
                delete = 'md',         -- Delete surrounding
                find = 'mf',           -- Find surrounding (to the right)
                find_left = 'mF',      -- Find surrounding (to the left)
                highlight = 'mh',      -- Highlight surrounding
                replace = 'mr',        -- Replace surrounding
                update_n_lines = 'mn', -- Update `n_lines`

                suffix_last = 'l',     -- Suffix to search with "prev" method
                suffix_next = 'n',     -- Suffix to search with "next" method
            }
        }

        require("mini.operators").setup {}
        require("mini.pairs").setup {}
        require("mini.bracketed").setup {}
        require("mini.splitjoin").setup {}
        require("mini.files").setup {
            -- Use `''` (empty string) to not create one.
            mappings = {
                close       = 'q',
                go_in       = 'r',
                go_in_plus  = 'R',
                go_out      = 'c',
                go_out_plus = 'C',
                mark_goto   = "'",
                mark_set    = 'm',
                reset       = '<BS>',
                reveal_cwd  = '@',
                show_help   = 'g?',
                synchronize = '=',
                trim_left   = '<',
                trim_right  = '>',
            },
        }

        vim.keymap.set("n", "<leader>gd", "<CMD> lua require('mini.diff').toggle_overlay() <CR>",
            { desc = "[G]it [D]iff" })
        vim.keymap.set("n", "<leader>ot", "<CMD> lua MiniFiles.open(vim.api.nvim_buf_get_name(0)) <CR>",
            { desc = "[O]pen file [T]ree" })
    end,
}
