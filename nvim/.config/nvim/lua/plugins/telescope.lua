local function current_word(case)
    return ":lua require('textcase').current_word('" .. case .. "')<CR>"
end

local function lsp_rename(case)
    return ":lua require('textcase').lsp_rename('" .. case .. "')<CR>"
end

local function operator(case)
    return ":lua require('textcase').operator('" .. case .. "')<CR>"
end

return {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.8",
    dependencies = {
        "nvim-lua/plenary.nvim",
        { "debugloop/telescope-undo.nvim" },
        { "aaronhallaert/advanced-git-search.nvim" },
        {
            "nvim-telescope/telescope-fzf-native.nvim",
            build = "make",
        },
        {
            "isak102/telescope-git-file-history.nvim",
            dependencies = {
                "nvim-lua/plenary.nvim",
                "tpope/vim-fugitive"
            }
        },
        {
            "johmsalas/text-case.nvim",
            keys = {
                -- Current word
                { "<leader>mtu",  current_word("to_upper_case"),    desc = "[U] TO UPPER CASE" },
                { "<leader>mtl",  current_word("to_lower_case"),    desc = "[L] to lower case" },
                { "<leader>mts",  current_word("to_snake_case"),    desc = "[S] to_snake_case" },
                { "<leader>mtk",  current_word("to_dash_case"),     desc = "[K] to-kebab-case" },
                { "<leader>mtn",  current_word("to_constant_case"), desc = "[N] TO_CONSTANT_CASE" },
                { "<leader>mtd",  current_word("to_dot_case"),      desc = "[D] to.dot.case" },
                { "<leader>mth",  current_word("to_phrase_case"),   desc = "[H] To phrase case" },
                { "<leader>mtc",  current_word("to_camel_case"),    desc = "[C] toCamelCase" },
                { "<leader>mtp",  current_word("to_pascal_case"),   desc = "[P] ToPascalCase" },
                { "<leader>mtt",  current_word("to_title_case"),    desc = "[T] To Title Case" },
                { "<leader>mtf",  current_word("to_path_case"),     desc = "[F] to/path/case" },

                -- LSP rename
                { "<leader>mtru", lsp_rename("to_upper_case"),      desc = "[U] TO UPPER CASE" },
                { "<leader>mtrl", lsp_rename("to_lower_case"),      desc = "[L] to lower case" },
                { "<leader>mtrs", lsp_rename("to_snake_case"),      desc = "[S] to_snake_case" },
                { "<leader>mtrk", lsp_rename("to_dash_case"),       desc = "[K] to-kebab-case" },
                { "<leader>mtrn", lsp_rename("to_constant_case"),   desc = "[N] TO_CONSTANT_CASE" },
                { "<leader>mtrd", lsp_rename("to_dot_case"),        desc = "[D] to.dot.case" },
                { "<leader>mtrh", lsp_rename("to_phrase_case"),     desc = "[H] To phrase case" },
                { "<leader>mtrc", lsp_rename("to_camel_case"),      desc = "[C] toCamelCase" },
                { "<leader>mtrp", lsp_rename("to_pascal_case"),     desc = "[P] ToPascalCase" },
                { "<leader>mtrt", lsp_rename("to_title_case"),      desc = "[T] To Title Case" },
                { "<leader>mtrf", lsp_rename("to_path_case"),       desc = "[F] to/path/case" },

                -- Operator
                { "<leader>mtou", lsp_rename("to_upper_case"),      desc = "[U] TO UPPER CASE" },
                { "<leader>mtol", lsp_rename("to_lower_case"),      desc = "[L] to lower case" },
                { "<leader>mtos", lsp_rename("to_snake_case"),      desc = "[S] to_snake_case" },
                { "<leader>mtok", lsp_rename("to_dash_case"),       desc = "[K] to-kebab-case" },
                { "<leader>mton", lsp_rename("to_constant_case"),   desc = "[N] TO_CONSTANT_CASE" },
                { "<leader>mtod", lsp_rename("to_dot_case"),        desc = "[D] to.dot.case" },
                { "<leader>mtoh", lsp_rename("to_phrase_case"),     desc = "[H] To phrase case" },
                { "<leader>mtoc", lsp_rename("to_camel_case"),      desc = "[C] toCamelCase" },
                { "<leader>mtop", lsp_rename("to_pascal_case"),     desc = "[P] ToPascalCase" },
                { "<leader>mtot", lsp_rename("to_title_case"),      desc = "[T] To Title Case" },
                { "<leader>mtof", lsp_rename("to_path_case"),       desc = "[F] to/path/case" },
            },
        },
    },
    config = function()
        local telescope = require("telescope")

        telescope.setup {
            pickers = {
                find_files = {
                    theme = "ivy",
                },
            },
            extensions = {
                fzf = {}
            }
        }

        local builtins = require("telescope.builtin")

        -- Loading extensions
        telescope.load_extension("fzf")
        telescope.load_extension("undo")
        telescope.load_extension("advanced_git_search")
        telescope.load_extension("git_file_history")
        telescope.load_extension("textcase")


        local function find_neovim_files(opts)
            builtins.find_files({ cwd = vim.fn.stdpath("config") })
        end

        local function find_pony_regtests(opts)
            builtins.find_files({ cwd = "/home/julien/files/Pony/pony-core/lib/regtests/scenarios_v2" })
        end

        local function find_pony_endpoints(opts)
            builtins.find_files({ cwd = "/home/julien/files/Pony/pony-core/endpoints" })
        end

        local function fuzzy_find(opts)
            builtins.current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
                winblend = 10,
                previewer = false,
            }))
        end

        -- Fuzzly search inside the current file
        vim.keymap.set("n", "<leader>sn", find_neovim_files, { desc = "[S]earch [N]eovim files" })
        vim.keymap.set("n", "<leader>/", fuzzy_find, { desc = "[/] Fuzzily search in current buffer" })

        -- Pony binds
        vim.keymap.set("n", "<leader>spr", find_pony_regtests, { desc = "[S]earch [P]ony [R]egtests" })
        vim.keymap.set("n", "<leader>spe", find_pony_endpoints, { desc = "[S]earch [P]ony [E]ndpoints" })

        require("config.telescope.multigrep").setup()
        require("config.telescope.git_cmds").setup()
        require("config.telescope.insert_template").setup()
        require("config.telescope.gitmoji").setup()
    end,
    keys = {
        -- Search keybinds
        { "<leader>so",  "<CMD> Telescope oldfiles <CR>",                             desc = "[S]earch recently [O]pened files" },
        { "<leader>sf",  "<CMD> Telescope find_files <CR>",                           desc = "[S]earch [F]iles" },
        { "<leader>sd",  "<CMD> Telescope diagnostics <CR>",                          desc = "[S]earch [D]iagnostics" },
        { "<leader>sh",  "<CMD> Telescope help_tags <CR>",                            desc = "[S]earch [H]elp" },
        { "<leader>sk",  "<CMD> Telescope keymaps <CR>",                              desc = "[S]earch [K]eymps" },
        { "<leader>su",  "<CMD> Telescope undo <CR>",                                 desc = "[S]earch [U]ndo tree" },
        { "<leader>st",  "<CMD> TextCaseOpenTelescope <CR>",                          desc = "[S]earch [T]ext case" },
        { "<leader>sb",  "<CMD> Telescope buffers <CR>",                              desc = "[S]earch [B]uffers" },

        -- Git stuff
        { "<leader>sgf", "<CMD> Telescope git_files <CR>",                            desc = "[S]earch [G]it project [F]iles" },
        { "<leader>sgd", "<CMD> Telescope advanced_git_search diff_commit_file <CR>", desc = "[S]earch [G]it [D]iff commit file" },
        { "<leader>sgb", "<CMD> Telescope git_branches <CR>",                         desc = "[S]earch [G]it [B]ranches" },
        { "<leader>sgs", "<CMD> Telescope git_status <CR>",                           desc = "[S]earch [G]it [S]tatus" },
        { "<leader>sgt", "<CMD> Telescope git_stash <CR>",                            desc = "[S]earch [G]it [S]tash" },
        { "<leader>sgc", "<CMD> Telescope git_commits <CR>",                          desc = "[S]earch [G]it [C]ommits" },
        { "<leader>sgi", "<CMD> Telescope git_bcommits <CR>",                         desc = "[S]earch [G]it branch comm[I]ts" },
        { "<leader>sgr", "<CMD> Telescope git_bcommits_range <CR>",                   desc = "[S]earch [G]it branch commits [R]ange" },
    },
}
