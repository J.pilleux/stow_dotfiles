return {
    'saghen/blink.cmp',
    dependencies = { "L3MON4D3/LuaSnip", version = "v2.*", build = "make install_jsregexp" },
    version = 'v0.*',
    opts = {
        keymap = {
            preset = 'default',
            ['<A-">'] = { function(cmp) cmp.accept({ index = 1 }) end },
            ['<A-«>'] = { function(cmp) cmp.accept({ index = 2 }) end },
            ['<A-»>'] = { function(cmp) cmp.accept({ index = 3 }) end },
            ['<A-(>'] = { function(cmp) cmp.accept({ index = 4 }) end },
            ['<A-)>'] = { function(cmp) cmp.accept({ index = 5 }) end },
            ['<A-@>'] = { function(cmp) cmp.accept({ index = 6 }) end },
            ['<A-+>'] = { function(cmp) cmp.accept({ index = 7 }) end },
            ['<A-->'] = { function(cmp) cmp.accept({ index = 8 }) end },
            ['<A-/>'] = { function(cmp) cmp.accept({ index = 9 }) end },
        },

        appearance = {
            use_nvim_cmp_as_default = true,
            nerd_font_variant = 'mono'
        },

        snippets = { preset = 'luasnip' },

        completion = {
            menu = {
                auto_show = true,
            },
        },

        sources = {
            default = { 'snippets', 'lsp', 'path', 'buffer' },
            providers = {
                snippets = {
                    score_offset = 100
                }
            }
        },
    },
}
