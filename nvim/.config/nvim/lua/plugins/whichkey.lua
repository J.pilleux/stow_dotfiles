return {
	"folke/which-key.nvim",
	event = "VeryLazy",
	init = function()
		vim.o.timeout = true
		vim.o.timeoutlen = 300
	end,
	opts = {
		-- your configuration comes here
		-- or leave it empty to use the default settings
		-- refer to the configuration section below
	},
	config = function()
		local presets = require("which-key.plugins.presets")
		presets.operators["c"] = nil

        local wk = require("which-key")
        wk.add({
                { "<leader>o", group = "[O]pen" },
                { "<leader>h", group = "[H]arpoon" },
                { "<leader>d", group = "[D]ebugger" },
                { "<leader>m", group = "[M]isc" },
                { "<leader>r", group = "[R]eplace" },
                { "<leader>v", group = "[V] Spectre" },

                { "<leader>l", group = "[L]SP" },
                { "<leader>lg", group = "[G]oto" },
                { "<leader>lw", group = "[W]orkspace" },

                { "<leader>g", group = "[G]it" },
                { "<leader>gf", group = "[F]ile" },
                { "<leader>gl", group = "[L]og" },
                { "<leader>gr", group = "[R]ebase" },
                { "<leader>gh", group = "[H]unk" },
                { "<leader>gb", group = "[B]lame" },
                { "<leader>gu", group = "B[U]ffer" },
                { "<leader>gc", group = "[C]onflict" },

                { "<leader>s", group = "[S]earch" },
                { "<leader>sg", group = "[G]it" },

                { "<leader>t", group = "[T]ests" },

                { "<leader>mt", group = "[T]ext case" },
                { "<leader>mtr", group = "LSP [R]ename" },
                { "<leader>mto", group = "[O]perator" },

                { "<leader>c", group = "[C]opilot"},
                {
                        mode = "v",
                        { "<leader>c", group = "[C]opilot" },
                }
        })
	end,
}
