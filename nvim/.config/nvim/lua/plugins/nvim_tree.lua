local function my_on_attach(bufnr)
	local api = require("nvim-tree.api")

	local function opts(desc)
		return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
	end

	-- default mappings
	api.config.mappings.default_on_attach(bufnr)

	-- custom mappings
	vim.keymap.set("n", "c", "h", opts("left"))
	vim.keymap.set("n", "t", "j", opts("down"))
	vim.keymap.set("n", "s", "k", opts("up"))
	vim.keymap.set("n", "r", "l", opts("right"))
	vim.keymap.set("n", "-", "8", opts("height"))
	vim.keymap.set("n", "<BS>", api.tree.change_root_to_parent, opts("Go to parent dir"))
end

return {
	"nvim-tree/nvim-tree.lua",
	config = function()
		require("nvim-tree").setup({
			on_attach = my_on_attach,
			sort = {
				sorter = "case_sensitive",
			},
			view = {
				width = 60,
			},
			renderer = {
				group_empty = true,
			},
			filters = {
				dotfiles = true,
			},
			update_focused_file = {
				enable = true
			}
		})
	end,
	keys = {
		{ "<Leader>ot", ":NvimTreeToggle<CR>", desc = "Open file [T]ree" },
	},
}
