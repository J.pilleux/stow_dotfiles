vim.api.nvim_create_autocmd("User", {
  pattern = "MiniFilesActionRename",
  callback = function(event)
    Snacks.rename.on_rename_file(event.data.from, event.data.to)
  end,
})

local toggle_dim = function ()
  local is_enabled = Snacks.dim.enabled
  if is_enabled then
    Snacks.dim.disable()
  else
    Snacks.dim.enable()
  end
end

return {
  "folke/snacks.nvim",
  priority = 1000,
  lazy = false,
  opts = {
    animate = { enabled = true },
    bigfile = { enabled = true },
    dim = { enabled = true },
    quickfile = { enabled = true },
    input = { enabled = true },
    notifier = {
      enabled = true,
      timeout = 3000,
    },
    statuscolumn = { enabled = true },
    words = { enabled = true },
    rename = { enabled = true }
  },
  config = function ()
    vim.keymap.set("n", "<leader><leader>d", toggle_dim, { desc = "Toggle [D]im" })
    vim.keymap.set("n", "<leader>S", function() Snacks.scratch.select() end , { desc = "[S]elect scratch buffer" })
  end
}
