return {
	"mfussenegger/nvim-dap",
	dependencies = {
		"rcarriga/nvim-dap-ui",
		"jay-babu/mason-nvim-dap.nvim",
		"nvim-neotest/nvim-nio",
		"leoluz/nvim-dap-go",
		"mfussenegger/nvim-dap-python",
	},
	config = function()
		local dap = require("dap")
		local dapui = require("dapui")
		dapui.setup()

		dap.listeners.before.attach.dapui_config = function()
			dapui.open()
		end
		dap.listeners.before.launch.dapui_config = function()
			dapui.open()
		end
		dap.listeners.before.event_terminated.dapui_config = function()
			dapui.close()
		end
		dap.listeners.before.event_exited.dapui_config = function()
			dapui.close()
		end

		vim.keymap.set("n", "<leader>db", dap.toggle_breakpoint, { desc = "Toggle [B]reakpoint" })
		vim.keymap.set("n", "<leader>dc", dap.continue, { desc = "[C]ontinue debugger" })

		dap.configurations.python = {
			{
				name = "Pytest: Current File",
				type = "python",
				request = "launch",
				module = "pytest",
				args = { "${file}", "-sv", "--log-cli-level=INFO" },
				console = "integratedTerminal",
			},
		}

		vim.keymap.set("n", "<leader>dm", "<CMD> lua require('dap-python').test_method()", { desc = "Test [M]ethod" })
		vim.keymap.set("n", "<leader>dl", "<CMD> lua require('dap-python').test_method()", { desc = "Test c[L]ass" })
		vim.keymap.set("v", "<leader>ds", "<CMD> lua require('dap-python').test_method()", { desc = "Debug [S]election" })

		require("dap-go").setup()
		local dap_python = require("dap-python")
		dap_python.setup("/home/julien/files/Pony/pony-core/venv/bin/python")
		dap_python.test_runner = "pytest"
	end,
}
