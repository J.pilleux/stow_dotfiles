return {
	"nvim-lua/plenary.nvim",
	"nvim-tree/nvim-web-devicons",

    "tpope/vim-sleuth",

	"dkprice/vim-easygrep",
	"gpanders/editorconfig.nvim",
	"vimwiki/vimwiki",
	"andreshazard/vim-freemarker", -- Plugin for .ftl files
}
