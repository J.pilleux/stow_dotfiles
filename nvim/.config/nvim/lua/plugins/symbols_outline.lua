return {
	"hedyhli/outline.nvim",
	config = function()
		vim.keymap.set("n", "<leader>oy", "<CMD> Outline <CR>", { desc = "[O]pen [S]ymbol outline" })

        require("outline").setup{}
	end,
}
