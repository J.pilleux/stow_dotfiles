return {
    "nvim-neotest/neotest",
    dependencies = {
        "nvim-neotest/nvim-nio",
        "nvim-lua/plenary.nvim",
        "antoinemadec/FixCursorHold.nvim",
        "nvim-treesitter/nvim-treesitter",

        "nvim-neotest/neotest-plenary",
        "nvim-neotest/neotest-python",
    },
    config = function()
        require("neotest").setup({
            adapters = {
                require("neotest-python")({
                    -- Extra arguments for nvim-dap configuration
                    -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
                    dap = { justMyCode = false },
                    -- Command line arguments for runner
                    -- Can also be a function to return dynamic values
                    args = { "--log-level", "DEBUG" },
                    -- Runner to use. Will use pytest if available by default.
                    -- Can be a function to return dynamic value.
                    runner = "pytest",
                    -- Custom python path for the runner.
                    -- Can be a string or a list of strings.
                    -- Can also be a function to return dynamic value.
                    -- If not provided, the path will be inferred by checking for
                    -- virtual envs in the local directory and for Pipenev/Poetry configs
                    -- python = "/home/julien/miniconda3/envs/pony-core/bin/python",
                    python = "python3.9",
                    -- Returns if a given file path is a test file.
                    -- NB: This function is called a lot so don't perform any heavy tasks within it.
                    -- !!EXPERIMENTAL!! Enable shelling out to `pytest` to discover test
                    -- instances for files containing a parametrize mark (default: false)
                    pytest_discover_instances = true,
                }),
                require("neotest-plenary"),
            },
        })
        vim.keymap.set("n", "<Leader>tr", "<CMD> Neotest run <CR>", { desc = "[T]est [R]un"})
        vim.keymap.set("n", "<Leader>to", "<CMD> Neotest output <CR>", { desc = "[T]est [O]utput"})
        vim.keymap.set("n", "<Leader>ts", "<CMD> Neotest summary <CR>", { desc = "[T]est [S]ummary"})
        vim.keymap.set("n", "<Leader>tj", "<CMD> Neotest jump <CR>", { desc = "[T]est [J]ump"})
        vim.keymap.set("n", "<Leader>tt", "<CMD> Neotest stop <CR>", { desc = "[T]est S[T]op"})
        vim.keymap.set("n", "<Leader>ta", "<CMD> Neotest attach <CR>", { desc = "[T]est [A]ttach"})
        vim.keymap.set("n", "<Leader>tp", "<CMD> Neotest output-panel <CR>", { desc = "[T]est output [P]annel"})
        vim.keymap.set("n", "<Leader>td", "<CMD> lua require('neotest').run.run({strategy = 'dap'}) <CR>", { desc = "[T]est run [D]AP"})
    end,
}
