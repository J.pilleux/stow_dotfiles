#!/bin/bash

curdate() {
    local cdate="$(date +"%d/%m")"
    echo " $cdate"
}

hour() {
    local chour="$(date +"%H:%M")"
    echo "󰥔 $chour"
}

battery() {
    local batleft="$(cat /sys/class/power_supply/BAT0/capacity)"
    echo " $batleft%"
}

cpuusageperc() {
    local cpu="$(mpstat | tail -1 | sed 's/  */ /g' | cut -d' ' -f3 | cut -d'.' -f1)"
    echo "󰍛 $cpu %"
}

memusageperc() {
    local mem="$(free | grep Mem | awk '{print int($3/$2 * 100.0)}')"
    echo " $mem %"
}

volume() {
    local vol="$(awk -F"[][]" '/dB/ { print $2 }' <(amixer sget Master))"
    echo "󰕾 $vol"
}

nitrogen --restore &
xscreensaver -no-splash &
picom &
dunst &
greenclip daemon &
flameshot &
nm-applet &

while true
do
    xsetroot -name " $(memusageperc) | $(cpuusageperc) | $(battery) | $(volume) | $(curdate) $(hour) | ";
    sleep 1;
done &

