# Void linux cheat sheet

## Package manager

### Search for packages

```bash
xbps-query -Rs <package-name>
```

### Install packages

```bash
xbps-install <package-list>
```
