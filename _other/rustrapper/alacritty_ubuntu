#!/usr/bin/env bash

function yellow {
    echo -e "\033[33m$1\033[0m"
}

function green {
    echo -e "\033[32m$1\033[0m"
}

function red {
    echo -e "\033[31m$1\033[0m"
}

function param {
    echo -e "   " $(green "$1") "$2"
}

function error {
    echo -e $(red "ERROR") ":" "$1"
}

ALACRITTY_PPA=ppa:aslatter/ppa

function usage {
    yellow "USAGE:"
    echo -e "    $(basename $0) [OPTIONS] [--]\n"

    yellow "OPTIONS:"
    param "-h, --help" "Display this message"
    param "-i, --install" "Install TO FILL"
    param "-u, --uninstall" "Uninstall TO FILL"
    param "-d, --describe" "Print a description of the script"
}

function install {
    if grep -qi 'ubuntu' <<< $(uname -a)
    then
        sudo apt update
        sudo add-apt-repository "$ALACRITTY_PPA"
        sudo apt -y install alacritty
    else
        echo $(red "Error") "This must be launched on Ubuntu"
    fi
}

function uninstall {
    sudo apt purge alacritty
}

function describe {
    echo -e "Add a PPA to install alacritty in Ubuntu:\n"
    echo "  PPA: $ALACRITTY_PPA"
}

#-----------------------------------------------------------------------
#  Handle command line arguments
#-----------------------------------------------------------------------

while getopts ":hiud" opt
do
  case $opt in

    h|help     ) usage; exit 0;;

    i|install  ) install; exit 0;;

    u|uninstall) uninstall; exit 0;;

    d|describe ) describe; exit 0;;

    ? ) error "Option '$OPTARG' need an argument";
        usage;
        exit 1 ;;

    * ) error "Option does not exist : $OPTARG\n";
        usage;
        exit 1 ;;

  esac    # --- end of case ---
done
shift $(($OPTIND-1))

usage
exit 0
