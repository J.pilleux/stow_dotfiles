#!/bin/bash
set -e

git submodule update --init

sudo apt install -y stow gcc make doas tmux zsh i3 cargo ripgrep fd-find fzf cmake

source ./packer.sh
source ./nvim.sh
source ./i3.sh
source ./librewolf.sh

pushd ${HOME}/.dotfiles
stow tmux fonts less usr_bin wallpapers
stow --adopt shells # I know this will fail because of .bashrc
git checkout .
popd

sudo groupadd wheel
sudo usermod ${USER} -a -G wheel
sudo usermod ${USER} -s /bin/zsh
echo "permit :wheel" > tmp
sudo mv tmp /etc/doas.conf

sudo sed -i.bak 's/XKBOPTIONS=""/XKBOPTIONS="ctrl:nocaps"/' /etc/default/keyboard
