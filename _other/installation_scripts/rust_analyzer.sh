#!/bin/bash
set -eu pipefail

curr_script=$(realpath ${0})
curr_dir=$(dirname ${curr_script})
source "${curr_dir}/utility_functions.sh"

_analyzer_prefix="${HOME}/.local/bin/rust-analyzer"
_analyzer_url="https://github.com/rust-analyzer/rust-analyzer/releases/latest/download/rust-analyzer-x86_64-unknown-linux-gnu.gz"

mkdir -p ${_analyzer_prefix}

check_dependencies "curl gunzip"

curl -L "${_analyzer_url}" | gunzip -c - > "${_analyzer_prefix}"
chmod +x ~/.local/bin/rust-analyzer
