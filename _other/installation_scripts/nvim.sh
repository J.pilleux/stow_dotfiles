#!/bin/bash
set -e

_NVIM_APPIMAGE=nvim.appimage
_NVIM_PREFIX=/opt/nvim/
_GIT_REPO=https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
_BIN_PATH=/usr/local/bin/

# sudo apt install -y libfuse2 g++

sudo mkdir -p "${_NVIM_PREFIX}"
wget "${_GIT_REPO}"
sudo chmod +x "${_NVIM_APPIMAGE}"
sudo mv "${_NVIM_APPIMAGE}" "${_NVIM_PREFIX}${_NVIM_APPIMAGE}"
sudo ln -s "${_NVIM_PREFIX}${_NVIM_APPIMAGE}" "${_BIN_PATH}nvim"

pushd ${HOME}/.dotfiles
stow nvim
popd
