#!/bin/bash
set -eu pipefail

curr_script=$(realpath ${0})
curr_dir=$(dirname ${curr_script})
source "${curr_dir}/utility_functions.sh"

_rustup_url="https://sh.rustup.rs"

check_dependencies "curl"
curl --proto '=https' --tlsv1.2 -sSf ${_rustup_url} | sh
