#!/bin/bash

# Installing basic programs
sudo pacman -Syu --noconfirm
sudo pacman -Sy --noconfirm base-devel gcc make tmux rust ripgrep fzf cmake wget i3-wm picom rofi feh openssh nodejs i3blocks

# Install yay
sudo git clone https://aur.archlinux.org/yay-git.git /opt/yay
sudo chown -R ${USER}:${USER} /opt/yay
pushd /opt/yay
makepkg -si
popd

yay -S alacritty-git stow-git nitrogen-git librewolf-bin i3lock-fancy-git ghcup-hs-bin rustup-git --nocleanmenu --answerdiff None

# PACKER
_PACKER_PREFIX=~/.local/share/nvim/site/pack/packer/start/packer.nvim

if [[ ! -f ${_PACKER_PREFIX} ]]; then
    git clone --depth 1 https://github.com/wbthomason/packer.nvim ${_PACKER_PREFIX}
fi

# NVIM
_NVIM_GIT_REPO=https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
sudo mkdir -p /etc/opt/neovim
wget "${_NVIM_GIT_REPO}"
sudo chmod +x ./nvim.appimage
sudo mv ./nvim.appimage /etc/opt/neovim/nvim
sudo ln -s /etc/opt/neovim/nvim /usr/local/bin

# STOW
pushd ~/.dotfiles/
/usr/bin/stow nvim i3 rofi alacritty tmux usr_bin wallpapers fonts
/usr/bin/stow --adopt bash
git checkout .
popd
