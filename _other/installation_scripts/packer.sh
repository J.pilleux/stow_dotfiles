#!/bin/bash

_PACKER_PREFIX=~/.local/share/nvim/site/pack/packer/start/packer.nvim

if [[ ! -f ${_PACKER_PREFIX} ]]; then
    git clone --depth 1 https://github.com/wbthomason/packer.nvim ${_PACKER_PREFIX}
fi
