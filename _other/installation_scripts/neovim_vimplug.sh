#!/bin/sh
set -eu pipefail

curr_script=$(realpath ${0})
curr_dir=$(dirname ${curr_script})
source "${curr_dir}/utility_functions.sh"

install_nvim() {
    check_dependencies "pip3"
    pip3 install neovim
    sprintf "Neovim installed"
}

install_vimplug() {
    check_dependencies "git nvim vim"
    sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    sprintf "Vimplug installed"
}

install_all() {
    install_nvim
    install_vimplug
    sprintf "Neovim and vimplug installed"
}

install_all

unset curr_script curr_dir
