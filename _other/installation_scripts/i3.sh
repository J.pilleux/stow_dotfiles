#!/bin/bash
set -e

echo "-- Installing dependencies"
sudo apt install -y wget i3 i3lock-fancy i3blocks rofi picom nitrogen flameshot bsdmainutils

pip3 install i3ipc
wget https://raw.githubusercontent.com/tmfink/i3-wk-switch/master/i3-wk-switch.py

pushd ${HOME}/.dotfiles
echo "-- Stowing packages"
stow i3 rofi alacritty
popd
