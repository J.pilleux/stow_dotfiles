#!/bin/sh

c_red="\e[31m"
c_grn="\e[32m"
c_stp="\e[0m"

eprintf() {
    local msg="${1}"
    echo -e "${c_red}ERROR${c_stp}: ${msg}"
}

sprintf() {
    local msg="${1}"
    echo -e "${c_grn}SUCCESS${c_stp}: ${msg}"
}

get_not_installed_progs() {
    local to_check="${1}"
    local not_found_progs=""

    for program in ${to_check}; do
        command -v ${program} > /dev/null || not_found_progs="${not_found_progs}${program} "
    done

    echo "${not_found_progs}"
}

check_dependencies() {
    local to_check="${1}"
    local not_installed=$(get_not_installed_progs "${to_check}")
    if [ -n "${not_installed}" ]; then
        eprintf "Dependencies not met: ${not_installed}"
        exit 1
    fi
}
