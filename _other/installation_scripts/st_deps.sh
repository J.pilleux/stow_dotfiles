#!/bin/bash
set -e

sudo apt install -y gcc make libx11-dev libxft-dev

pushd ${HOME}/.dotfiles/_other/suckless/st/
make
sudo make install
popd

