# Color key:
#   #1d1f21 Background new : #1A1B26
#   #282a2e Current Line
#   #373b41 Selection
#   #c5c8c6 Foreground
#   #969896 Comment
#   #cc6666 Red
#   #de935f Orange
#   #f0c674 Yellow
#   #b5bd68 Green
#   #8abeb7 Aqua
#   #81a2be Blue
#   #b294bb Purple


## set status bar
set -g status-style bg="#1A1B26",fg="#A9B1D6"

## highlight active window
setw -g window-style bg=default
setw -g window-active-style bg=default
setw -g pane-active-border-style ''

## highlight activity in status bar
setw -g window-status-activity-style fg="#7DCFFF"
setw -g window-status-activity-style bg="#1A1B26"

## pane border and colors
set -g pane-active-border-style bg=default
set -g pane-active-border-style fg="#414868"
set -g pane-border-style bg=default
set -g pane-border-style fg="#414868"

set -g clock-mode-colour "#7AA2F7"
set -g clock-mode-style 24

set -g message-style bg="#7DCFFF"
set -g message-style fg="#9ECE6A"

set -g message-command-style bg="#7DCFFF"
set -g message-command-style fg="#9ECE6A"

# message bar or "prompt"
set -g message-style bg="#1A1B26"
set -g message-style fg="#BB9AF7"

set -g mode-style bg="#1A1B26"
set -g mode-style fg="#F7768E"

# right side of status bar holds "[host name] (date time)"
set -g status-right-length 100
set -g status-right-style fg=black
set -g status-right-style bold
set -g status-right '#[fg=#9ECE6A,bg=#2d2d2d] %H:%M |#[fg=#BB9AF7] %d.%m.%y '

# make background window look like white tab
set-window-option -g window-status-style bg=default
set-window-option -g window-status-style fg="#A9B1D6"
set-window-option -g window-status-style none
set-window-option -g window-status-format '#[fg=#BB9AF7,bg=colour235] #I #[fg=#A9B1D6,bg=#2d2d2d] #W #[default]'

# make foreground window look like bold yellow foreground tab
set-window-option -g window-status-current-style none
set-window-option -g window-status-current-format '#[fg=#9ECE6A,bg=#2d2d2d] #I #[fg=#A9B1D6,bg=#414868] #W #[default]'

# active terminal yellow border, non-active white
set -g pane-border-style bg=default
set -g pane-border-style fg="#414868"
set -g pane-active-border-style fg="#9ECE6A"

