## Tmux configuration ##

# Automatically set the window title with the running program
set-window-option -g automatic-rename on
set-option -g set-titles on
set-option -g focus-events on

# No delay for escape key
set -sg escape-time 0

# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator

# decide whether we're in a Vim process
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"

bind-key -n 'M-c' if-shell "$is_vim" 'send-keys M-c' 'select-pane -L'
bind-key -n 'M-t' if-shell "$is_vim" 'send-keys M-t' 'select-pane -D'
bind-key -n 'M-s' if-shell "$is_vim" 'send-keys M-s' 'select-pane -U'
bind-key -n 'M-r' if-shell "$is_vim" 'send-keys M-r' 'select-pane -R'

tmux_version='$(tmux -V | sed -En "s/^tmux ([0-9]+(.[0-9]+)?).*/\1/p")'

if-shell -b '[ "$(echo "$tmux_version < 3.0" | bc)" = 1 ]' \
    "bind-key -n 'M-\\' if-shell \"$is_vim\" 'send-keys M-\\'  'select-pane -l'"
if-shell -b '[ "$(echo "$tmux_version >= 3.0" | bc)" = 1 ]' \
    "bind-key -n 'M-\\' if-shell \"$is_vim\" 'send-keys M-\\\\'  'select-pane -l'"

bind-key -n 'M-Space' if-shell "$is_vim" 'send-keys M-Space' 'select-pane -t:.+'

set-option -s set-clipboard off

bind P paste-buffer
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi V send-keys -X rectangle-toggle
unbind -T copy-mode-vi Enter
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'wl-copy'
bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel 'wl-copy'
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'wl-copy'

bind-key -T copy-mode-vi 'M-c' select-pane -L
bind-key -T copy-mode-vi 'M-t' select-pane -D
bind-key -T copy-mode-vi 'M-s' select-pane -U
bind-key -T copy-mode-vi 'M-r' select-pane -R
bind-key -T copy-mode-vi 'M-\' select-pane -l
bind-key -T copy-mode-vi 'M-Space' select-pane -t:.+

bind-key -r i run-shell "tmux neww tmux-cht.sh"

# Activate the pane synchronization
bind-key -r a set-window-option synchronize-panes
set -ag status-right '#{?pane_synchronized, #[fg=red]IN_SYNC#[default],}'

bind n new-window

# Maximize/Minimize a pane
bind m resize-pane -Z

bind k kill-pane
bind C-k kill-window

# Easy reload configuration with ^L
bind C-l source-file ~/.config/tmux/tmux.conf
# Mouse mode
set -g mouse on

# # Use vim keybindings in copy mode
setw -g mode-keys vi

# Binding v to copy mode
unbind [
bind v copy-mode

bind-key -T copy-mode-vi c send-keys -X cursor-left
bind-key -T copy-mode-vi t send-keys -X cursor-down
bind-key -T copy-mode-vi s send-keys -X cursor-up
bind-key -T copy-mode-vi r send-keys -X cursor-right

bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
bind -n WheelDownPane select-pane -t= \; send-keys -M
bind -n C-WheelUpPane select-pane -t= \; copy-mode -e \; send-keys -M

bind -T copy-mode-vi    C-WheelUpPane   send-keys -X halfpage-up
bind -T copy-mode-vi    C-WheelDownPane send-keys -X halfpage-down

bind -T copy-mode-vi u send-keys -X halfpage-up
bind -T copy-mode-vi d send-keys -X halfpage-down

bind -T copy-mode-emacs C-WheelUpPane   send-keys -X halfpage-up
bind -T copy-mode-emacs C-WheelDownPane send-keys -X halfpage-down

bind-key -T copy-mode a send-keys -X cancel

# Test for rebind the switch layouts
bind M-'"' select-layout even-horizontal
bind M-«   select-layout even-vertical
bind M-»   select-layout main-horizontal
bind M-(   select-layout main-vertical
bind M-)   select-layout tiled

# Switch windows
bind -n M-q   select-window -t 1
bind -n M-g   select-window -t 2
bind -n M-h   select-window -t 3
bind -n M-f   select-window -t 4
bind -n M-)   select-window -t 5
bind -n M-@   select-window -t 6
bind -n M-+   select-window -t 7
bind -n M--   select-window -t 8
bind -n M-/   select-window -t 9
bind -n M-*   select-window -t 0
bind -n M-$   select-window -l

# Create new session
bind -n C-M-s    new

# Switch sessions
bind -n C-M-q  , user_id: str,  switch -t 0
bind -n C-M-g   switch -t 1
bind -n C-M-h   switch -t 2
bind -n C-M-f   switch -t 3

# Changing split commands
bind | split-window -h
bind - split-window -v
unbind %

# Start windows and panes at 1 and not 0
set -g base-index 1
setw -g pane-base-index 1

# Switch windows with shift arrows
bind -n S-Left previous-window
bind -n S-Right next-window

## Tmux theme ##
# Undercurl
set-option -g default-terminal "screen-256color"
set-option -sa terminal-overrides ',screen-256color:RGB'
set-option -qa terminal-overrides ',screen-256color:Tc'

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-copycat'

# Initialize TMUX plugin manager
run '~/.config/tmux/plugins/tpm/tpm'

# Resize panes with vim binds with alt key being held on
bind-key -r -T prefix   C-c      resize-pane -L 5
bind-key -r -T prefix   C-t      resize-pane -D 5
bind-key -r -T prefix   C-s      resize-pane -U 5
bind-key -r -T prefix   C-r      resize-pane -R 5

set-option -g status-position top
set -g status on
set -g status-interval 1
set -g status-justify centre

set -g status-style bg=black,fg=white
setw -g window-status-current-style fg=green,bg=black

set -g status-left ' #S '
set -g status-right ' %H:%M '
set-window-option -g window-status-format " #I] #W "
set-window-option -g window-status-current-format " #I] #W "

