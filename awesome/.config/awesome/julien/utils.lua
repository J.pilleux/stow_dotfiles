local M = {}

M.check_file_exists = function (fname)
    local file = io.open(fname, "r")
    if file ~= nil then
        io.close(file)
        return true
    end
    return false
end

return M
