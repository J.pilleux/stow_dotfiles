local utils = require "julien.utils"
local awful = require "awful"

local M = {}

M.spawn_init = function()
    -- Automatically start applications
    awful.spawn.with_shell("picom")
    awful.spawn.with_shell("nitrogen --restore")
    awful.spawn.with_shell("xscreensaver -no-splash &")
    awful.spawn.with_shell("dunst")
    awful.spawn.with_shell("nm-applet")
    awful.spawn.with_shell("blueman-applet")

    local home = os.getenv("HOME")
    local xmodmap_file = home .. "/.Xmodmap"
    if utils.check_file_exists(xmodmap_file) then
        awful.spawn.with_shell("xmodmap " .. xmodmap_file)
    end
end

return M
