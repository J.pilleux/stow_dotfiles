local wibox = require("wibox")

local kbdcfg = {}

kbdcfg.cmd = "setxkbmap"
kbdcfg.layout = { { "fr", "bepo", "BEPO" }, { "fr", "", " FR " }, { "us", "", " US " } }
kbdcfg.current = 1 -- us is our default layout
kbdcfg.widget = wibox.widget.textbox()
kbdcfg.widget.markup = ' <span foreground="#a6e3a1">' .. kbdcfg.layout[kbdcfg.current][3] .. '</span> '
kbdcfg.switch = function()
    kbdcfg.current = kbdcfg.current % #(kbdcfg.layout) + 1
    local t = kbdcfg.layout[kbdcfg.current]
    kbdcfg.widget.markup = ' <span foreground="#a6e3a1">' .. t[3] .. '</span> '
    os.execute(kbdcfg.cmd .. " " .. t[1] .. " " .. t[2])
end

-- Mouse bindings
-- kbdcfg.widget:buttons(
--     awful.util.table.join(awful.button({}, 1, function() kbdcfg.switch() end))
-- )

return kbdcfg
