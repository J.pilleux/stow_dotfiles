local gears = require("gears")
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local sharedtags = require("awesome-sharedtags")

local M = {}

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
local modkey = "Mod4"

-- {{{ Key bindings
local gkeys = gears.table.join(
-- Open programs
    awful.key({ modkey, }, "Return", function() awful.spawn(default_apps.terminal) end,
        { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey, "Control" }, "b", function() awful.spawn(default_apps.browser) end,
        { description = "open a web browser", group = "launcher" }),
    awful.key({ modkey, "Control" }, "c", function() awful.spawn(default_apps.chat) end,
        { description = "open a chat client", group = "launcher" }),
    awful.key({ modkey, "Control" }, "m", function() awful.spawn(default_apps.mail_client) end,
        { description = "open a mail client", group = "launcher" }),
    awful.key({ modkey, "Control" }, "s", function() awful.spawn("steam") end,
        { description = "open a steam", group = "launcher" }),

    awful.key({ modkey, }, "o", function() awful.spawn("rofi -show window") end,
        { description = "focus the next screen", group = "screen" }),

    awful.key({ modkey, }, "u", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),
    awful.key({ modkey, }, "Left", awful.tag.viewprev, { description = "view previous", group = "tag" }),
    awful.key({ modkey, }, "Right", awful.tag.viewnext, { description = "view next", group = "tag" }),
    awful.key({ modkey, }, "Escape", awful.tag.history.restore, { description = "go back", group = "tag" }),

    awful.key({ modkey, }, "t", function() awful.client.focus.byidx(1) end,
        { description = "focus next by index", group = "client" }),
    awful.key({ modkey, }, "s", function() awful.client.focus.byidx(-1) end,
        { description = "focus previous by index", group = "client" }),
    awful.key({ modkey, }, "w", function() mymainmenu:show() end, { description = "show main menu", group = "awesome" })
    ,

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "t", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "s", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, }, "Tab", function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, }, ":", function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey, }, "v", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" })
    ,
    -- awful.key({ modkey,           }, "Tab", function () awful.client.focus.history.previous() if client.focus then client.focus:raise() end end, {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey, "Control", "Shift" }, "r", awesome.restart, { description = "reload awesome", group = "awesome" })
    ,
    awful.key({ modkey, "Control", "Shift" }, "q", awesome.quit, { description = "quit awesome", group = "awesome" }),

    awful.key({ modkey, }, "r", function() awful.tag.incmwfact(0.05) end,
        { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey, }, "c", function() awful.tag.incmwfact(-0.05) end,
        { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "r", function() awful.tag.incnmaster(-1, nil, true) end,
        { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "c", function() awful.tag.incnmaster(1, nil, true) end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey, }, "space", function() awful.layout.inc(1) end, { description = "select next", group = "layout" })
    ,
    awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }),
    --awful.key({ modkey, "Control" }, "c",     function () awful.tag.incncol( 1, nil, true)    end, {description = "increase the number of columns", group = "layout"}),
    --awful.key({ modkey, "Control" }, "r",     function () awful.tag.incncol(-1, nil, true)    end, {description = "decrease the number of columns", group = "layout"}),

    awful.key({ modkey, }, "l", function() awful.spawn("xscreensaver-command -lock") end,
        { description = "lock the screen", group = "screen" }),

    awful.key({ modkey, "Shift" }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", { raise = true }
                )
            end
        end,
        { description = "restore minimized", group = "client" }),

    -- Prompt
    awful.key({ modkey }, "p", function() awful.util.spawn("rofi -display-drun 'Run:' -show drun") end,
        { description = "run prompt", group = "launcher" }),

    awful.key({ modkey }, "x",
        function()
            awful.prompt.run {
                prompt       = "Run Lua code: ",
                textbox      = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        { description = "lua execute prompt", group = "awesome" }),
    -- Menubar
    awful.key({}, "XF86AudioRaiseVolume", function() awful.util.spawn("amixer set Master 5%+") end),
    awful.key({}, "XF86AudioLowerVolume", function() awful.util.spawn("amixer set Master 5%-") end),
    awful.key({}, "XF86AudioMute", function() volume_widget:toggle() end),
    awful.key({}, "Print", function() awful.util.spawn("flameshot gui") end),

    -- Shift-Alt to change keyboard layout
    awful.key({ modkey, "Shift" }, "l", function() kbdcfg.switch() end)
)
--
-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    gkeys = gears.table.join(gkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = tags[i]
                if tag then
                    sharedtags.viewonly(tag, screen)
                end
            end,
            { description = "view tag #" .. i, group = "tag" }),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = tags[i]
                if tag then
                    sharedtags.viewtoggle(tag, screen)
                end
            end,
            { description = "toggle tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tag" }),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "tag" })
    )
end

local clientkeys = gears.table.join(awful.key({ modkey, }, "f", function(c) c.fullscreen = not c.fullscreen c:raise() end,
    { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function(c) c:kill() end, { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space", awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "client" }),
    awful.key({ modkey, "Shift" }, "o", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "client" }),
    awful.key({ modkey, }, "n", function(c)
        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        c.minimized = true
    end, { description = "minimize", group = "client" }),
    awful.key({ modkey, }, "m", function(c) c.maximized = not c.maximized c:raise() end,
        { description = "(un)maximize", group = "client" }),
    awful.key({ modkey, "Control" }, "m", function(c) c.maximized_vertical = not c.maximized_vertical c:raise() end,
        { description = "(un)maximize vertically", group = "client" }),
    awful.key({ modkey, "Shift" }, "m", function(c) c.maximized_horizontal = not c.maximized_horizontal c:raise() end,
        { description = "(un)maximize horizontally", group = "client" })
)

local clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

M.globalkeys = gkeys
M.modkey = modkey
M.clientkeys = clientkeys
M.clientbuttons = clientbuttons

return M
