local beautiful = require("beautiful")

local M = {}

-- Load Debian menu entries
local has_fdo, freedesktop = pcall(require, "freedesktop")
local default_apps = require("julien.default_apps")

-- {{{ Menu
-- Create a launcher widget and a main menu
local myawesomemenu = {
    { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
    { "manual", default_apps.terminal .. " -e man awesome" },
    { "edit config", default_apps.editor_cmd .. " " .. awesome.conffile },
    { "restart", awesome.restart },
    { "quit", function() awesome.quit() end },
}

local menu_awesome = { "awesome", myawesomemenu, beautiful.awesome_icon }
local menu_terminal = { "open terminal", default_apps.terminal }
M.mymainmenu = {}

if has_fdo then
    M.mymainmenu = freedesktop.menu.build({
        before = { menu_awesome },
        after = { menu_terminal }
    })
else
    M.mymainmenu = awful.menu({
        items = {
            menu_awesome,
            menu_terminal,
        }
    })
end


return M
