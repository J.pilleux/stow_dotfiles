local M = {}

local default_term = "kitty"
local default_editor = os.getenv("EDITOR") or "nvim"

M.terminal = default_term
M.editor = default_editor
M.editor_cmd = default_term .. " -e " .. default_editor
M.browser = "floorp"
M.chat = "discord"
M.mail_client = "thunderbird"

return M
