-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")

local awful = require("awful")
require("awful.autofocus")
require("awful.hotkeys_popup.keys")
local hotkeys_popup = require("awful.hotkeys_popup")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local menubar = require("menubar")

local sharedtags = require("awesome-sharedtags")

-- Custom imports
require("julien.layouts")
require("julien.error_handling")
local default_apps = require("julien.default_apps")

-- Themes define colours, icons, font and wallpapers.
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
beautiful.init(gears.filesystem.get_configuration_dir() .. "tokyonights.lua")

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
local modkey = "Mod4"

local menus = require("julien.menus")
local mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = menus.mymainmenu })

-- Menubar configuration
menubar.utils.terminal = default_apps.terminal -- Set the terminal for aplications that require it

local kbdcfg = require("julien.keyboard")

local separator_widget = wibox.widget.textbox()
separator_widget.markup = '<span foreground="#585b70"> <b>></b> </span>'

local space_widget = wibox.widget.textbox()
space_widget.markup = " | "
-- {{{ Wibar
-- Create a textclock widget
local mytextclock = wibox.widget.textclock()
mytextclock.format = ' <span foreground="#89b4fa">%a %b %d %H:%M</span> '

local HOME = os.getenv("HOME")
-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t)
        t:view_only()
    end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({}, 4, function(t)
        awful.tag.viewnext(t.screen)
    end),
    awful.button({}, 5, function(t)
        awful.tag.viewprev(t.screen)
    end)
)

local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal("request::activate", "tasklist", { raise = true })
        end
    end),
    awful.button({}, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end)
)

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

local cpu_widget = require("custom-widgets.custom-cpu-widget.cpu-widget")
local volume_widget = require("awesome-wm-widgets.pactl-widget.volume")
local ram_widget = require("custom-widgets.custom-ram-widget.ram-widget")
local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")

local tags = sharedtags({
    { name = "    ", layout = awful.layout.layouts[1] },
    { name = "    ", layout = awful.layout.layouts[1] },
    { name = "    ", layout = awful.layout.layouts[1] },
    { name = "    ", layout = awful.layout.layouts[1] },
    { name = "    ", layout = awful.layout.layouts[1] },
    { name = "    ", layout = awful.layout.layouts[1] },
    { name = "    ", layout = awful.layout.layouts[1] },
})

local HOME = os.getenv("HOME")
local arc_icon_path = HOME .. "/.config/awesome/arc-icon-theme/Arc/status/symbolic/"

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    sharedtags.viewonly(tags[1], s)
    sharedtags.viewonly(tags[2], s)
    sharedtags.viewonly(tags[3], s)
    sharedtags.viewonly(tags[4], s)
    sharedtags.viewonly(tags[5], s)
    sharedtags.viewonly(tags[6], s)
    sharedtags.viewonly(tags[7], s)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({}, 1, function()
            awful.layout.inc(1)
        end),
        awful.button({}, 3, function()
            awful.layout.inc(-1)
        end),
        awful.button({}, 4, function()
            awful.layout.inc(1)
        end),
        awful.button({}, 5, function()
            awful.layout.inc(-1)
        end)
    ))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist({
        screen = s,
        filter = awful.widget.taglist.filter.all,
        buttons = taglist_buttons,
    })

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist({
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
    })

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup({
        layout = wibox.layout.align.horizontal,
        {
            -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        {
            -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            space_widget,
            kbdcfg.widget,
            separator_widget,
            cpu_widget(),
            separator_widget,
            ram_widget(),
            separator_widget,
            volume_widget({
                widget_type = "horizontal_bar",
                main_color = "#cdd6f4",
            }),
            separator_widget,
            batteryarc_widget({
                path_to_icons = arc_icon_path,
            }),
            separator_widget,
            mytextclock,
            s.mylayoutbox,
        },
    })
end)
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
-- Open programs
    awful.key({ modkey }, "Return", function()
        awful.spawn(default_apps.terminal)
    end, { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey, "Control" }, "b", function()
        awful.spawn(default_apps.browser)
    end, { description = "open a web browser", group = "launcher" }),
    awful.key({ modkey, "Control" }, "c", function()
        awful.spawn(default_apps.chat)
    end, { description = "open a chat client", group = "launcher" }),
    awful.key({ modkey, "Control" }, "m", function()
        awful.spawn(default_apps.mail_client)
    end, { description = "open a mail client", group = "launcher" }),
    awful.key({ modkey, "Control" }, "s", function()
        awful.spawn("steam")
    end, { description = "open a steam", group = "launcher" }),
    awful.key({ modkey }, "o", function()
        awful.spawn("rofi -show window")
    end, { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey }, "BackSpace", function()
        awful.spawn("rofi -show power-menu -modi power-menu:~/.local/mybin/rofi-power-menu.sh")
    end, { description = "Spawn power menu", group = "launcher" }),
    awful.key({ modkey }, "u", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),
    awful.key({ modkey }, "Left", awful.tag.viewprev, { description = "view previous", group = "tag" }),
    awful.key({ modkey }, "Right", awful.tag.viewnext, { description = "view next", group = "tag" }),
    awful.key({ modkey }, "Escape", awful.tag.history.restore, { description = "go back", group = "tag" }),
    awful.key({ modkey }, "t", function()
        awful.client.focus.byidx(1)
    end, { description = "focus next by index", group = "client" }),
    awful.key({ modkey }, "s", function()
        awful.client.focus.byidx(-1)
    end, { description = "focus previous by index", group = "client" }),
    awful.key({ modkey }, "w", function()
        mymainmenu:show()
    end, { description = "show main menu", group = "awesome" }),

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "t", function()
        awful.client.swap.byidx(1)
    end, { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "s", function()
        awful.client.swap.byidx(-1)
    end, { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey }, "Tab", function()
        awful.screen.focus_relative(1)
    end, { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey }, ":", function()
        awful.screen.focus_relative(-1)
    end, { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey }, "v", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),
    -- awful.key({ modkey,           }, "Tab", function () awful.client.focus.history.previous() if client.focus then client.focus:raise() end end, {description = "go back", group = "client"}),

    -- Standard program
    awful.key(
        { modkey, "Control", "Shift" },
        "r",
        awesome.restart,
        { description = "reload awesome", group = "awesome" }
    ),
    awful.key({ modkey, "Control", "Shift" }, "q", awesome.quit, { description = "quit awesome", group = "awesome" }),
    awful.key({ modkey }, "r", function()
        awful.tag.incmwfact(0.05)
    end, { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey }, "c", function()
        awful.tag.incmwfact(-0.05)
    end, { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "r", function()
        awful.tag.incnmaster(-1, nil, true)
    end, { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "c", function()
        awful.tag.incnmaster(1, nil, true)
    end, { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey }, "space", function()
        awful.layout.inc(1)
    end, { description = "select next", group = "layout" }),
    awful.key({ modkey, "Shift" }, "space", function()
        awful.layout.inc(-1)
    end, { description = "select previous", group = "layout" }),
    awful.key({ modkey }, "l", function()
        awful.spawn("xscreensaver-command -lock")
    end, { description = "lock the screen", group = "screen" }),

    awful.key({ modkey, "Shift" }, "n", function()
        local c = awful.client.restore()
        -- Focus restored client
        if c then
            c:emit_signal("request::activate", "key.unminimize", { raise = true })
        end
    end, { description = "restore minimized", group = "client" }),

    -- Prompt
    awful.key({ modkey }, "p", function()
        awful.util.spawn("rofi -display-drun 'Run:' -show drun")
    end, { description = "run prompt", group = "launcher" }),

    awful.key({ modkey }, "x", function()
        awful.prompt.run({
            prompt = "Run Lua code: ",
            textbox = awful.screen.focused().mypromptbox.widget,
            exe_callback = awful.util.eval,
            history_path = awful.util.get_cache_dir() .. "/history_eval",
        })
    end, { description = "lua execute prompt", group = "awesome" }),
    -- Menubar
    awful.key({}, "XF86AudioRaiseVolume", function()
        awful.util.spawn("amixer set Master 5%+")
    end),
    awful.key({}, "XF86AudioLowerVolume", function()
        awful.util.spawn("amixer set Master 5%-")
    end),
    awful.key({}, "XF86AudioMute", function()
        volume_widget:toggle()
    end),
    awful.key({}, "Print", function()
        awful.util.spawn("flameshot gui")
    end),

    -- Meta-Shift-L to change keyboard layout
    awful.key({ modkey, "Shift" }, "l", function()
        kbdcfg.switch()
    end)
)

local clientkeys = gears.table.join(
    awful.key({ modkey }, "f", function(c)
        c.fullscreen = not c.fullscreen
        c:raise()
    end, { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey }, "k", function(c)
        c:kill()
    end, { description = "close", group = "client" }),
    awful.key(
        { modkey, "Control" },
        "space",
        awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }
    ),
    awful.key({ modkey, "Control" }, "Return", function(c)
        c:swap(awful.client.getmaster())
    end, { description = "move to master", group = "client" }),
    awful.key({ modkey, "Shift" }, "o", function(c)
        c:move_to_screen()
    end, { description = "move to screen", group = "client" }),
    awful.key({ modkey }, "n", function(c)
        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        c.minimized = true
    end, { description = "minimize", group = "client" }),
    awful.key({ modkey }, "m", function(c)
        c.maximized = not c.maximized
        c:raise()
    end, { description = "(un)maximize", group = "client" }),
    awful.key({ modkey, "Control" }, "m", function(c)
        c.maximized_vertical = not c.maximized_vertical
        c:raise()
    end, { description = "(un)maximize vertically", group = "client" }),
    awful.key({ modkey, "Shift" }, "m", function(c)
        c.maximized_horizontal = not c.maximized_horizontal
        c:raise()
    end, { description = "(un)maximize horizontally", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(
        globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9, function()
            local screen = awful.screen.focused()
            local tag = tags[i]
            if tag then
                sharedtags.viewonly(tag, screen)
            end
        end, { description = "view tag #" .. i, group = "tag" }),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9, function()
            local screen = awful.screen.focused()
            local tag = tags[i]
            if tag then
                sharedtags.viewtoggle(tag, screen)
            end
        end, { description = "toggle tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9, function()
            if client.focus then
                local tag = tags[i]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end, { description = "move focused client to tag #" .. i, group = "tag" }),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
            if client.focus then
                local tag = tags[i]
                if tag then
                    client.focus:toggle_tag(tag)
                end
            end
        end, { description = "toggle focused client on tag #" .. i, group = "tag" })
    )
end

local clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen,
        },
    },
    { rule = { class = "Firefox" }, properties = { opacity = 1, maximized = false, floating = false } },
    { rule = { class = "Slack" }, properties = { opacity = 1, maximized = false, floating = false } },
    { rule = { class = "WebCord" }, properties = { opacity = 1, maximized = false, floating = false } },
    {
        rule = { class = "Dunst" },
        properties = { floating = true },
    },
    {
        rule = { class = "Discord" },
        properties = { tag = tags[" 1 "] },
    },
    {
        rule = { class = "Kitty" },
        properties = { tag = tags[" 3 "] },
    },
    {
        rule = { class = "zoom" },
        properties = { floating = true },
    },
    -- Floating clients.
    -- {
    --     rule_any = {
    --         instance = {
    --             "DTA", -- Firefox addon DownThemAll.
    --             "copyq", -- Includes session name in class.
    --             "pinentry",
    --         },
    --         class = {
    --             "Arandr",
    --             "Blueman-manager",
    --             "Gpick",
    --             "Kruler",
    --             "MessageWin", -- kalarm.
    --             "Sxiv",
    --             "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
    --             "Wpa_gui",
    --             "veromix",
    --             "xtightvncviewer"
    --         },
    --
    --         -- Note that the name property shown in xprop might be set slightly after creation of the client
    --         -- and the name shown there might not match defined rules here.
    --         name = {
    --             "Event Tester", -- xev.
    --         },
    --         role = {
    --             "AlarmWindow", -- Thunderbird's calendar.
    --             "ConfigManager", -- Thunderbird's about:config.
    --             "pop-up",    -- e.g. Google Chrome's (detached) Developer Tools.
    --         }
    --     },
    --     properties = { floating = true }
    -- },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = false },
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    awful.titlebar(c):setup({
        {
            -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout = wibox.layout.fixed.horizontal,
        },
        {
            -- Middle
            {
                -- Title
                align = "center",
                widget = awful.titlebar.widget.titlewidget(c),
            },
            buttons = buttons,
            layout = wibox.layout.flex.horizontal,
        },
        {
            -- Right
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.ontopbutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal(),
        },
        layout = wibox.layout.align.horizontal,
    })
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c)
    c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
    c.border_color = beautiful.border_normal
end)

require("julien.spawns").spawn_init()
