;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Julien Pilleux"
      user-mail-address "julien@jpilleux.xyz")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; Prefered themes are commented out

;;(setq doom-theme 'doom-city-lights)
;;(setq doom-theme 'doom-dracula)
;;(setq doom-theme 'doom-henna)
;;(setq doom-theme 'doom-laserwave)
;;(setq doom-theme 'doom-monokai-machine)
;;(setq doom-theme 'doom-monokai-octagon)
;;(setq doom-theme 'doom-monokai-spectrum)
;;(setq doom-theme 'doom-moonlight)
;;(setq doom-theme 'doom-oceanic-next)
(setq doom-theme 'doom-palenight)
;;(setq doom-theme 'doom-snazzy)
;;(setq doom-theme 'doom-spacegrey)
;;(setq doom-theme 'doom-vibrant)
;;(setq doom-theme 'doom-tokyo-night)

(setq doom-font (font-spec :family "Source Code Pro Semibold" :size 12))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

(add-hook! 'lsp-mode-hook 'lsp-lens-mode)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(setq projectile-project-search-path '("~/files/projects/"))

(after! lsp-rust
  (setq lsp-rust-server 'rust-analyzer))

(setq undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob. Be more granular
      auto-save-default t                         ; Nobody likes to loose work, I certainly don't
      truncate-string-ellipsis "…"                ; Unicode ellispis are nicer than "...", and also save /precious/ space
      password-cache-expiry nil                   ; I can trust my computers ... can't I?
      scroll-preserve-screen-position 'always     ; Don't have `point' jump around
      scroll-margin 2)                            ; It's nice to maintain a little margin

(display-time-mode 1)                             ; Enable time in the mode-line

(global-subword-mode 1)                           ; Iterate through CamelCase words

(setq evil-vsplit-window-right t
      evil-split-window-below t)

;; Setup org publish
(setq org-publish-project-alist
      `(("notes"
         :base-directory "~/notes"
         :recursive t
         :publishing-directory "~/notes/"
         :publishing-function org-html-publish-to-html)))
