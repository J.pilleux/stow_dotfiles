#!/bin/bash

_is_bepo=$(setxkbmap -query | grep bepo)

function yellow {
    echo -e "\033[33m$1\033[0m"
}

function green {
    echo -e "\033[32m$1\033[0m"
}

function red {
    echo -e "\033[31m$1\033[0m"
}

function param {
    echo -e "   " $(green "$1") "$2"
}

function error {
    echo -e $(red "ERROR") ":" "$1"
}

function usage {
    yellow "USAGE:"
    echo -e "    $0 [OPTIONS] [--]\n"

    yellow "OPTIONS:"
    param "-h, --help" "Display this message"
}

function get_current {
    if [[ -n ${_is_bepo} ]]; then
        echo "Keyboard: BEPO"
        echo "Kbd: BEPO"
        echo "#00FF00"
    else
        echo "Keyboard: US"
        echo "Kbd: US"
        echo "#FF0000"
    fi
}

function switch_layout {
    if [[ -n ${_is_bepo} ]]; then
        setxkbmap us -option "esc:swapescape"
    else
        setxkbmap fr bepo -option "esc:swapescape"
    fi
}


#-----------------------------------------------------------------------
#  Handle command line arguments
#-----------------------------------------------------------------------

while getopts ":hgs" opt
do
  case $opt in

    h|help     ) usage; exit 0 ;;

    g|get      ) get_current ;;

    s|switch   ) switch_layout ;;

    ? ) error "Option '$OPTARG' need an argument";
        usage;
        exit 1 ;;

    * ) error "Option does not exist : $OPTARG\n";
        usage;
        exit 1 ;;

  esac    # --- end of case ---
done
shift $(($OPTIND-1))

exit 0
