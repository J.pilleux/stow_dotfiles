#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: $0 <package_count>"
    exit 1
fi

X="$1"

sudo pacman -Sy
updates=$(pacman -Qu | awk '{print $1}' | head -n "$X" | tr '\n' ' ')

if [ -z "$updates" ]; then 
    echo "No update available."
    exit 0
fi

sudo pacman -S --needed "${updates}"
