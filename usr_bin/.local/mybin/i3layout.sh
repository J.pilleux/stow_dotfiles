#!/bin/sh

layout=$(i3-msg -t get_tree | jq -r '.. | select(.focused? == true) | .layout')
echo "$layout"
