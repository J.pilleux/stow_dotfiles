#!/bin/sh

KBD=$(setxkbmap -query | grep bepo)

function switch_layout {
    if [[ -z ${KBD} ]]; then
        setxkbmap fr bepo
    else
        setxkbmap us
    fi
}

if [[ -z ${KBD} ]]; then
    echo "󰌌   US "
    echo "󰌌   US "
else
    echo "󰌌  BEPO"
    echo "󰌌  BEPO"
fi

echo "#bb9af7"

case $BLOCK_BUTTON in
    1) # Left click
        switch_layout
esac

exit 0
