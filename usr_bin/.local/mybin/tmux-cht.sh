#!/usr/bin/env bash

selected=`cat ~/.config/tmux/tmux-cht-languages ~/.config/tmux/tmux-cht-commands | fzf`
if [[ -z ${selected} ]]; then
    exit 0
fi

read -p "Enter Query for [ ${selected} ]: " query

if grep -qs "${selected}" ~/.config/tmux/tmux-cht-languages; then
    query=`echo $query | tr ' ' '+'`
    tmux neww bash -c "echo \"curl cht.sh/${selected}/${query}/\" & curl cht.sh/${selected}/${query} | less -Rn"
else
    tmux neww bash -c "curl -s cht.sh/${selected}~${query} | less -Rn"
fi
