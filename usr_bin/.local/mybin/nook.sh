#!/bin/bash

_process=${1}

nook_process() {
    local _process=${1}
    ps aux | grep -i "${_process}" | grep -v "nook" | tr -s ' ' | cut -d ' ' -f 2 | xargs kill -9 2> /dev/null
    echo "The process ${_process} should be killed"
}

nook_process "${1}"
