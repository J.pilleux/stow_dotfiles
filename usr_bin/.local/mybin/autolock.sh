#!/bin/sh

exec xautolock -detectsleep -time 5 -locker "i3lock -t -i ~/.local/wallpapers/0000.jpg" -notify 30 -notifier "notify-send -u critical -t 10000 -- 'LOCKING screen in 30 seconds'"
