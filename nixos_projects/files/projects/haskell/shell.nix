with (import <nixpkgs> {});

mkShell {
    name = "Haskell dev";
    nativeBuildInputs = [ ghc stack cabal-install haskellPackages.haskell-language-server ];
    libraryPkgconfigDepends = [ zlib ];

    shellHook = ''
        export HOSTNAME=Haskell
    '';
}
