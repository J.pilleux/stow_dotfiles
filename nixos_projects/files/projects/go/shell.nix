with (import <nixpkgs> {});

mkShell {
    nativeBuildInputs = [
        go
        golangci-lint-langserver
        golangci-lint
    ];
}
