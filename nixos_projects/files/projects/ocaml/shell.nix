with (import <nixpkgs> {});

mkShell {
    nativeBuildInputs = [
        ocaml
        opam
        ocamlformat
        dune_3 
        gmp
        ocamlPackages.conduit
    ];
}
