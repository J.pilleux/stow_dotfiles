# Stow dotfiles

A collection of stow packages of my dotfiles

## Disclaimer

Most of the configuration files here are tailored to fit a « bépo » keyboard layout (french Dvorak)
If anyone willing to use those, pay attention to the key bind part, which you will need to tweak if you do not use a bépo keyboard.

## Requirements

To use theses dotfiles, you will need this programs installed :

- Git (Version >= 2.16+);
- Stow.

## Program configurations

Here is the list of configurations :

| Stow package names | Description                                                                 |
|--------------------|-----------------------------------------------------------------------------|
| alacritty          | Alacrity terminal config                                                    |
| apt                | files to use the testing and unstable branches                              |
| awesome            | Awesome window manager configuration                                        |
| desktop            | some configuration files for .desktop configuration used by lightdm         |
| doom_emacs         | Configuration for a doomed emacs                                            |
| emacs              | My config for emacs (pretty empty, ngl)                                     |
| ideavim            | Some vim wanabe editor configuration (Pycharm vim plugin, ewww)             |
| nvim               | NeoVim configuration, version 0.7+ required, will NOT work with regular Vim |
| qtile              | Experimental configuration for qtile window manager, probably wacky         |
| rofi               | Rofi menu bar configuration, again pretty experimental                      |
| suckless           | Some custom suckless programs with some patchs, like the bépo ones          |
| shells             | Bash AND Zsh (with oh-my-zsh) configuration, with aliases and stuff         |
| usr_bin            | Some custom scripts directly in $PATH (may be required by tmux)             |
| xmonad             | Xmonad window manager configuration, probably pretty broken                 |
| i3                 | i3 desktop manager, this one is actively maintained                         |
| less               | Rebinds for bépo keyboard layout                                            |
| tmux               | Tmux adapted to bépo keyboard layout. Need version 3+                       |
| dunst              | Some configuration for an extensible notification manager                   |
| wallpapers         | wallpapers !                                                                |
| fonts              | fonts !                                                                     |


## Basic installation

In order to use this dotfiles, you need to clone the repository and put it in a folder in your home directory :

```bash
git clone --recursive https://git.jpilleux.xyz/jpilleux/stow_dotfiles ${HOME}/.dotfiles
```

Then, you can install stow packages with this command :

```bash
stow PACKAGE_NAMES
```

## Other folder

The folder \_other does not contains any dotfile, but scripts or submodules that I use to install in a fresh install of Linux.
The scripts and submodules in this folder may require other programs not listed in the Requirements chapter.

## Troubleshooting

### Conflicts with already existing dotfiles

If when you stow a packages (like shells) and you got a "conflict" error, you have two options :
- Delete the files / folders in conflict
- Use the following commands :
```bash
stow --adopt PACKAGE_NAME
git checkout .
```

