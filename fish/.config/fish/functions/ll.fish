function ll --wraps=ls --wraps='exa -ll' --description 'alias ll=exa -ll'
  exa -ll $argv; 
end
