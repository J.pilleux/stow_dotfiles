function cback --wraps='cd ~/files/project/backend; conda activate nermind-backend' --wraps='cd ~/files/projects/backend; conda activate nermind-backend' --description 'alias cback=cd ~/files/projects/backend; conda activate nermind-backend'
  cd ~/files/projects/backend; conda activate nermind-backend $argv; 
end
