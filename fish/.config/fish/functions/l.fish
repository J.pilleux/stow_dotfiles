function l --wraps='ls -l' --wraps='exa -l' --description 'alias l=exa -l'
  exa -l $argv; 
end
