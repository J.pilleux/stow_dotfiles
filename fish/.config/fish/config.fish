if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -U fish_greeting
set -Ux EDITOR nvim

function get_status_color
    set _status $status

    if test $status -eq 0
        echo (set_color grey) $_status
    else
        echo (set_color red) $_status
    end
end

function fish_prompt -d "Write out the prompt"
    set -g fish_prompt_pwd_dir_length 16
    set -g __fish_git_prompt_char_upstream_prefix ' '

    printf '%s[ %s%s@%s%s %s] %s%s%s $ ' \
    (set_color yellow) \
    (set_color green) $USER \
    (set_color cyan) $hostname \
    (set_color yellow) \
    (set_color red) (prompt_pwd) (set_color normal) 
end

function fish_right_prompt
    set -g __fish_git_prompt_char_upstream_prefix ' '

    printf '%s%s%s %s%s%s' \
    (get_status_color) \
    (set_color purple) (fish_git_prompt) \
    (set_color normal) \
    (set_color grey) (date '+%H:%M')
end

# Colors
set fish_color_command '#c0caf5'
set fish_color_autosuggestion grey
set fish_color_option purple
set fish_color_param purple
set fish_color_valid_path blue

set fish_pager_color_progress yellow --background=black
set fish_pager_color_description blue

# Paths
if test -d "$HOME/.tmuxifier/bin"
    set -gx PATH "$HOME/.tmuxifier/bin" $PATH
    eval (tmuxifier init - fish)
end

if test -d "$HOME/.cabal/bin"
    set -gx PATH "$HOME/.cabal/bin" $PATH
end

if test -d "$HOME/.ghcup/bin"
    set -gx PATH "$HOME/.ghcup/bin" $PATH
end

bind \b backward-kill-word

if status is-interactive
    if type -q pfetch
        pfetch
    end
end

if test -f ~/.tmuxifier/bin/ 
    fish_add_path ~/.tmuxifier/bin
    eval (tmuxifier init - fish)
end

if test -f "$HOME/.config/nvm"
    fish_add_path "~/.config/nvm"
end

# Aliases
# Git aliases
abbr --add gst 'git status'
abbr --add ga 'git add -A'
abbr --add gca 'git commit -v -a'
abbr --add gam 'git commit -v -a --amend'
abbr --add gp 'git push'
abbr --add gl 'git pull'
abbr --add glg 'git log --oneline'

if type -q nvim
    abbr --add vim 'nvim'
    abbr --add v. 'nvim .'
    abbr --add v 'nvim'

    bind \cn 'nvim'
end

if type -q exa
    abbr --add ls 'exa'
    abbr --add l 'exa -l'
    abbr --add ll 'exa -l'
    abbr --add la 'exa -a'
else
    abbr --add l 'ls -l'
    abbr --add ll 'ls -l'
    abbr --add la 'ls -a'
end

if type -q bat
    abbr --add cat 'bat'
end

abbr --add less 'less --Rni'

# Human readable flags
abbr --add df 'df -r'
abbr --add free 'free -m'

abbr --add rr 'curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

abbr --add cdd 'cd ~/.dotfiles'
