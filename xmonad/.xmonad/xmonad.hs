import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops

import XMonad.Util.EZConfig
-- NOTE: Only needed for versions < 0.18.0! For 0.18.0 and up, this is
-- already included in the XMonad import and will give you a warning!
import XMonad.Util.Ungrab

import XMonad.Layout.ThreeColumns

myLayout = tiled ||| Mirror tiled ||| Full ||| ThreeColMid 1 (3/100) (1/2)
  where
    tiled   = Tall nmaster delta ratio
    nmaster = 1      -- Default number of windows in the master pane
    ratio   = 1/2    -- Default proportion of screen occupied by master pane
    delta   = 3/100  -- Percent of screen to increment by when resizing panes

myConfig = def
    { modMask = mod4Mask
    , handleEventHook = fullscreenEventHook
    , layoutHook = myLayout
    }

main :: IO ()
main = xmonad $ myConfig
    `additionalKeysP`
    [ ("M-l", spawn "xscreensaver-command -lock")
    , ("<Print>", spawn "flameshot gui"         )
    , ("M-b"  , spawn "floorp"                  )
    ]

