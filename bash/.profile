#!/usr/bin/env sh

if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

export GTK_THEME=Adwaita:dark
export GTK2_RC_FILES="/usr/share/themes/Adwaita-dark/gtk-2.0/gtkrc"
export QT_STYLE_OVERRIDE=Adwaita-Dark
. "$HOME/.cargo/env"
