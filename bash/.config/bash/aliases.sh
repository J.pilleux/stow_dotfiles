# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
alias cdd='cd ~/.dotfiles'

# Emacs
alias em='\emacs -nw'
if [[ -d "${HOME}/.doom.d" ]]
then
    alias doomsync="~/.emacs.d/bin/doom sync"
    alias doomdoctor="~/.emacs.d/bin/doom doctor"
    alias doomupgrade="~/.emacs.d/bin/doom upgrade"
    alias doompurge="~/.emacs.d/bin/doom purge"
fi

# git aliases
alias gst="git status"
alias ga="git add -A"
alias gca="git commit -v -a"
alias gaa="git add --all"
alias gca!="git commit -v -a --amend"
alias gp="git push"
alias gl="git pull"
alias glg="git log --oneline"
alias gm="git merge"
alias gma="git merge --abort"
alias gci='git commit --edit -m "$(git log -1 --pretty=%B)"'

alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias tm="tmux -f ~/.config/tmux/tmux.conf"
alias cnv="cd ~/.dotfiles/nvim_light/.config/nvim/ && nvim"

alias src="source ./venv/bin/activate"

# Vim = Neovim
if type nvim &> /dev/null
then
    alias vim="nvim"
    alias v.="nvim ."
    alias v="nvim"
fi

# Ls substitute
if command -v exa > /dev/null
then
    alias ls="exa"
    alias l="exa -l"
    alias ll="exa -l"
    alias la="exa -a"
else
    alias l="ls -l"
    alias ll="ls -l"
    alias la="ls -a"
fi

# Cat substitute
if command -v bat > /dev/null
then
    alias cat="bat"
fi

# Dotfile management
alias dotpull="git -C ~/.dotfiles pull"

alias less="less -Rni"

# add the ssh yubikey to the ssh agent
alias sshadd="ssh-add -s /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so"

# adding human readable flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

alias luamake='/home/julien/lua-language-server/3rd/luamake/luamake'

# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

alias ciao='sudo shutdown -h now'

alias ca='conda activate $(conda info --envs | tail -n +3 | grep -v base | awk "NF!=0 { print \$1 }" | fzf)'
