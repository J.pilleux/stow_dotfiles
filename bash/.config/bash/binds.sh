#!/usr/bin/env bash

if type "nvim" > /dev/null 2>&1; then
    bind -x '"\C-n": nvim'
else
    bind -x '"\C-n": echo ERROR - Bind unavailable: NeoVim required'
fi

if type "fd" > /dev/null 2>&1 && type "fzf" > /dev/null 2>&1; then
    bind -x '"\C-f": fd | fzf'
else
    bind -x '"\C-f": echo ERROR - Bind unavailable: fd-find AND fzf required'
    bind -x '"\C-f": echo ERROR - Bind unavailable: fd-find AND fzf required'
fi

if type "tmuxifier" > /dev/null 2>&1; then
    bind -x '"\C-t": tmuxifier s $(tmuxifier ls | fzf)'
else
    bind -x '"\C-t": echo ERROR - Bind unavailable: tmuxifier required'
fi

