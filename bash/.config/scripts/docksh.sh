#!/bin/bash

function dockit {
    if [[ "${#}" != "2" ]] && [[ "${#}" != "1" ]]
    then
        echo "Usage: dockit <docker_image> <cmd>"  
        echo
        echo "Options:"
        echo
        echo "  docker_image  A running docker image"
        echo "  cmd           An interactive command to run inside the docker image (defaluts to 'bash')"
        return
    fi

    local _container="${1}"
    local _cmd="${2:-bash}"

    docker exec -it ${_container} ${_cmd}
}

_dockit_completion() {
    local curr=${COMP_WORDS[COMP_CWORD]}
    local names=$(docker ps --format '{{.Names}}')
    COMPREPLY=( $(compgen -W "$names" -- $curr) )
}

complete -F _dockit_completion dockit

