#!/usr/bin/env bash

dockhost() {
    if [[ "${#}" != "1" ]]; then
        echo -e "\033[mERROR\033[m: You need the enter a container name"
        echo "Usage: dockhost <container_name>"
        return
    fi
    local container_name=${1}
    DOCKER_IP=$(docker inspect "${container_name}" | jq -r '.[0].NetworkSettings.Networks | to_entries[] | .value.IPAddress')

    if ! grep -q "${container_name}$" /etc/hosts ; then
        echo "No ${container_name} line found, appending existing /etc/hosts with ip ${DOCKER_IP}"
        echo
        sed "$ a ${DOCKER_IP}  ${container_name}" /etc/hosts

        echo 
        echo "Is this what you want? (yes/no)"
        read -r REPLY
        echo
        if [[ $REPLY =~ ^[Yy]$ ]]; then
            sudo sed -i "$ a ${DOCKER_IP}  ${container_name}" /etc/hosts
            echo "/etc/hosts Appended"
        else
            echo "Operation aborted"
        fi
    else
        echo "${container_name} found in /etc/hosts, will replace with ip: ${DOCKER_IP}"
        echo
        sed "s/^.*${container_name}$/${DOCKER_IP}\t${container_name}/" /etc/hosts

        echo 
        echo "Is this what you want? (yes/no)"
        read -r REPLY
        echo
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
            sudo sed -i "s/^.*${container_name}$/${DOCKER_IP}\t${container_name}/" /etc/hosts
            echo "Successfully substituted"
        else
            echo "Operation aborted"
        fi
    fi
}

_dockhost_completion() {
    local curr=${COMP_WORDS[COMP_CWORD]}
    local names=$(docker ps --format '{{.Names}}')
    COMPREPLY=( $(compgen -W "$names" -- $curr) )
}

# complete -F _dockhost_completion
