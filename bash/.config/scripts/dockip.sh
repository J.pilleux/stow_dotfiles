#! /bin/bash

dockip() {
    if [[ "${#}" != "1" ]]
    then
        echo -e "Usage: dockip <docker_name>"
        return 1
    fi

    DOCKER_NAME=${1}

    DOCKER_IP=$(docker inspect "${DOCKER_NAME}" | jq -r '.[0].NetworkSettings.Networks | to_entries[] | .value.IPAddress')
    echo -n "${DOCKER_IP}" | xclip -sel clip
    echo -e "Docker image ${DOCKER_NAME} has IP ${DOCKER_IP} (Copied to clipboard)"
}

_dockip_completion() {
    local curr=${COMP_WORDS[COMP_CWORD]}
    local names=$(docker ps --format '{{.Names}}')
    COMPREPLY=( $(compgen -W "$names" -- $curr) )
}

complete -F _dockip_completion dockip

