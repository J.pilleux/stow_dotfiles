# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

export EDITOR="nvim"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_RUNTIME_DIR="/run/user/$UID"

# ---- XDG directories compatibility START
# BASH HISTORY
export HISTFILE="${XDG_STATE_HOME}"/bash/history
! test -f "${XDG_STATE_HOME}/bash" && mkdir -p "${XDG_STATE_HOME}/bash"

# CABAL
export CABAL_CONFIG="$XDG_CONFIG_HOME"/cabal/config
export CABAL_DIR="$XDG_DATA_HOME"/cabal

# CARGO
export CARGO_HOME="$XDG_DATA_HOME"/cargo

# DOCKER
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker

# DOOM EMACS
export DOOMDIR="$XDG_CACHE_HOME/doom"

# GHCUP
export GHCUP_USE_XDG_DIRS=true

# GNUPGP
export GNUPGHOME="$XDG_DATA_HOME"/gnupg

# GO
export GOPATH="${XDG_DATA_HOME}/go"

# NODE
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history

# RUSTUP
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup

# STACK
export STACK_ROOT="$XDG_DATA_HOME"/stack

export _Z_DATA="$XDG_DATA_HOME/z"

# LESS
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

# ---- XDG directories compatibility END

export MYSSHGIT="ssh://git@gitlab.com/J.pilleux"
export MYHTTPGET="https://gitlab.com/J.pilleux"
export TERM=screen-256color

# Lang specific
export PATH="${PATH}:~/.ghcup/bin:~/.cabal/bin"

# Trying to export PATH with go bin
test -d /usr/local/go/bin && export PATH="${PATH}:/usr/local/go/bin"

# include .bashrc if it exists
# test -f "${HOME}/.bashrc" && source "${HOME}/.bashrc"

# set PATH so it includes user's private bin if it exists
test -d "${HOME}/bin" && PATH="${HOME}/bin:${PATH}"

# set PATH so it includes user's private bin if it exists
test -d "${HOME}/.local/bin" && PATH="${HOME}/.local/bin:${PATH}"

# set PATH so it includes user's private bin if it exists
test -d "${HOME}/.local/usr_bin" && PATH="${HOME}/.local/usr_bin:${PATH}"

# Start automatically ssh agent
if [[ -n "${XDG_RUNTIME_DIR}" ]]; then
    if [ -f "${XDG_RUNTIME_DIR}/ssh-agent.env" ]
    then
        if ! pgrep -u "${USER}" ssh-agent > /dev/null
        then
            ssh-agent -t 1h > "${XDG_RUNTIME_DIR}/ssh-agent.env"
        fi
        if [[ ! "${SSH_AUTH_SOCK}" ]]; then
            source "${XDG_RUNTIME_DIR}/ssh-agent.env" >/dev/null
            export SSH_AUTH_SOCK="/run/user/$UID/yubikey-agent/yubikey-agent.sock"
        fi
    fi
fi
