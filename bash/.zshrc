# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.config/zsh/oh-my-zsh"

set -o emacs

# Load the git information (required in order to display it in the shell)
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '%b '

setopt PROMPT_SUBST
PROMPT='%F{green}%*%f %F{blue}%~%f %F{red}${vcs_info_msg_0_}%f$ '

plugins=(git z)

# User configuration

# Showing dotfiles with tab completion
setopt globdots

XDG_CONFIG_HOME="${HOME}/.config"

# Sourcing all my garbage
test -f "${XDG_CONFIG_HOME}/bash/functions.sh" && source "${XDG_CONFIG_HOME}/bash/functions.sh"
test -f "${XDG_CONFIG_HOME}/bash/aliases.sh" && source "${XDG_CONFIG_HOME}/bash/aliases.sh"
test -f "${HOME}/.override.sh" && source "${HOME}/.override.sh"
test -f "${HOME}/.cargo/env" && source "${HOME}/.cargo/env"
test -f "${HOME}/.local/mybin" && source "${HOME}/.local/mybin"
test -d "${XDG_CONFIG_HOME}/scripts/" && for f in ${XDG_CONFIG_HOME}/scripts/*.sh; do source "$f"; done
test -f "/nix/var/nix/profiles/default/etc/profile.d/nix.sh" && source "/nix/var/nix/profiles/default/etc/profile.d/nix.sh"

export PATH="${HOME}/.local/mybin/:${HOME}/.yarn/bin:${HOME}/.config/yarn/global/node_modules/.bin:${HOME}/.tmuxifier/bin:${PATH}"
export NVM_DIR="$HOME/.config/nvm"

eval "$(tmuxifier init -)"

export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

bindkey -s '^v' 'nvim ^M'

if type "pfetch" > /dev/null 2>&1; then
    pfetch
fi
