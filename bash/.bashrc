#!/usr/bin/env sh

[[ $- != *i* ]] && return

function src_part {
    local fname="${1}"
    test -f "${fname}" && . "${fname}"
}

# Idk what this does.
xhost +local:root > /dev/null 2>&1
complete -cf sudo

export HISTSIZE=50000
export HISTFILESIZE=50000

shopt -s checkwinsize  # This helps the "interactives shell displays" to properly render.
shopt -s expand_aliases  # Makes the defined alises expand the existing aliase list.
shopt -s histappend  # Enable history appending instead of overwriting.

# Prompt

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="\[\e[33m\][\[\e[m\] \[\e[32m\]\u\[\e[m\]@\[\e[36m\]\h\[\e[m\] \[\e[33m\]]\[\e[m\] \[\e[31m\]\w\[\e[m\] \[\e[35m\]\$(parse_git_branch)\[\e[00m\] \\$ "
export HISTTIMEFORMAT="%d/%m/%y %T "

_rustrapper_completion() {
    test ${#COMP_WORDS[@]} -gt 3 && return

    local typed="${COMP_WORDS[1]}"
    local need_completion=("describe" "edit" "install" "uninstall" )
    if [[ " ${need_completion[*]} " == *" $typed "* ]]
    then
        local test=$(ls "$HOME/.dotfiles/_other/rustrapper/")
        COMPREPLY=($(compgen -W "$test" "${COMP_WORDS[2]}"))
    else
        test ${#COMP_WORDS[@]} -gt 2 && return
        COMPREPLY=($(compgen -W "describe edit help install list multiple new uninstall" "${COMP_WORDS[1]}"))
    fi
}

complete -F _rustrapper_completion rustrapper

XDG_CONFIG_HOME="${HOME}/.config"

src_part "${XDG_CONFIG_HOME}/bash/functions.sh"
src_part "${XDG_CONFIG_HOME}/bash/aliases.sh"
src_part "${XDG_CONFIG_HOME}/bash/binds.sh"
src_part "${HOME}/.override.sh"
src_part "${HOME}/.cargo/env"
src_part "${HOME}/.local/mybin"
src_part "${XDG_CONFIG_HOME}/bash/z.sh"
src_part "/home/julien/.local/share/ghcup/env" # ghcup-env
src_part "/usr/share/bash-completion/completions/git"

test -d "${XDG_CONFIG_HOME}/scripts/" && for f in "${XDG_CONFIG_HOME}/scripts/*.sh"; do source $f; done

export PATH="${HOME}/.local/mybin/:${HOME}/.yarn/bin:${HOME}/.config/yarn/global/node_modules/.bin:${HOME}/.tmuxifier/bin:${PATH}:/usr/local/go/bin"
export TERM=screen-256color
export NVM_DIR="$HOME/.config/nvm"
export EDITOR=nvim
export GPG_TTY=$(tty)

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    dbus-run-session Hyprland
fi

if type "pfetch" > /dev/null 2>&1; then
    pfetch
fi

if type "tmuxifier" > /dev/null 2>&1; then
    eval "$(tmuxifier init -)"
fi

if type "rbenv" > /dev/null 2>&1; then
    eval "$(rbenv init - bash)"
fi

# pnpm
export PNPM_HOME="/home/julienpilleux/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

. "$HOME/.cargo/env"
